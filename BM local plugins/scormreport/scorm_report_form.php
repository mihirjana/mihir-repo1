<?php

/**
 * Handles uploading files
 *
 * @package    my scorm report
 * @copyright  Arjun Singh <arjunsingh@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');
}
require_once($CFG->libdir.'/formslib.php');
class scorm_report_form extends moodleform {
    function definition() {
        global $CFG,$DB,$USER,$PAGE,$OUTPUT;
        $crsid = optional_param('crsid', 0 ,PARAM_INT);
        $mform =& $this->_form;
        $users = array();
        $courses = $DB->get_records('course');
        unset($courses[1]);
        $course = [];
        foreach($courses as $key=>$course1){
            $category = $DB->get_record('course_categories',array('id'=>$course1->category));
            $course[$course1->id] = $category->name.'  -  '.$course1->fullname;
            unset($course[1]);
        }
        $mform->addElement('select', 'courseid',get_string('courseid','local_scormreport'), $course, array('single'));
        $mform->setType('courseid',PARAM_RAW);   
        $this->add_action_buttons();

    }
}
