<?php

/**
 * Scorm Report
 * @author     Arjun Singh <arjunsingh@elearn10.com>
 * @package    local_scormreport
 * @copyright  23/08/2017 lms of india
 * @license    http://lmsofindia.com/
 */

defined('MOODLE_INTERNAL') || die;

$plugin->version = 2015081550;
$plugin->maturity = MATURITY_STABLE;
$plugin->requires = 2011120501; // Moodle 2.2 release and upwards
$plugin->component = 'local_scormreport';
