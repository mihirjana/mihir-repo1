<?php
$PAGE->requires->js(new 
	moodle_url($CFG->wwwroot.'/local/scormreport/datatable/javascript/jquery.dataTables.min.js'), 
	true);
$PAGE->requires->js(new 
	moodle_url($CFG->wwwroot.'/local/scormreport/datatable/javascript/dataTables.bootstrap.js'),true);
$PAGE->requires->js(new 
	moodle_url($CFG->wwwroot.'/local/scormreport/datatable/javascript/buttons.flash.min.js'),true);
$PAGE->requires->js(new 
	moodle_url($CFG->wwwroot.'/local/scormreport/datatable/javascript/buttons.html5.min.js'),true);
$PAGE->requires->js(new 
	moodle_url($CFG->wwwroot.'/local/scormreport/datatable/javascript/buttons.print.min.js'),true);
$PAGE->requires->js(new 
	moodle_url($CFG->wwwroot.'/local/scormreport/datatable/javascript/jszip.min.js'),true);
$PAGE->requires->js(new 
	moodle_url($CFG->wwwroot.'/local/scormreport/datatable/javascript/pdfmake.min.js'),true);
$PAGE->requires->js(new 
	moodle_url($CFG->wwwroot.'/local/scormreport/datatable/javascript/vfs_fonts.js'),true);
$PAGE->requires->js(new 
	moodle_url($CFG->wwwroot.'/local/scormreport/datatable/javascript/dataTables.buttons.min.js'),true);