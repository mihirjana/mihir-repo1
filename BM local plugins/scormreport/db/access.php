<?php

/**
 * Scorm Report Access File
 * @author     Arjun Singh <arjunsingh@elearn10.com>
 * @package    local_scormreport
 * @copyright  23/08/2017 lms of india
 * @license    http://lmsofindia.com/
 */

defined('MOODLE_INTERNAL') || die;

$capabilities = array(

	'local/scormreport:manage' => array(

		'riskbitmask' => RISK_SPAM,

		'captype' => 'write',
		'contextlevel' => CONTEXT_COURSE,
		'archetypes' => array(
			'editingteacher' => CAP_ALLOW,
			'manager' => CAP_ALLOW,
		),
	),
);