<?php

/**
 * Scorm Report View Page
 * @author     Arjun Singh <arjunsingh@elearn10.com>
 * @package    local_scormreport
 * @copyright  23/08/2017 lms of india
 * @license    http://lmsofindia.com/
 */

require_once('../../config.php');
require_once('scorm_report_form.php');
require_once($CFG->libdir . '/formslib.php');
require('csslink.php');
require('jslink.php');
require_login(0,false);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/scormreport/view.php');
$title = 'Scorm Report';
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->navbar->add($title);
$PAGE->requires->jquery('pagination.js');
echo $OUTPUT->header();  
$modids = array(); 
$mform = new scorm_report_form();
if ($mform->is_cancelled()){
	redirect(new moodle_url('/local/scormreport/view.php', array()));
} else if ($data = $mform->get_data()) {

	$sql = "SELECT cm.id,cm.instance
	from mdl_modules m
	INNER join mdl_course_modules cm 
	on m.id = cm.module
	where cm.course = $data->courseid;";
	$module = $DB->get_records_sql($sql);
	$sql1 = "SELECT ue.userid as uid
	FROM {enrol} e
	LEFT JOIN {user_enrolments} ue on e.id = ue.enrolid AND ue.timeend < NOW()
	WHERE e.courseid = $data->courseid AND e.enrol ='manual' AND e.status = 0
	GROUP by ue.userid";
	$enrolledusers = $DB->get_records_sql($sql1);
	$modids = array();
	$userids = array();
	if($module){
		foreach($module as $mid){
			if($enrolledusers)
			{	
				foreach($enrolledusers as $userid){
					$acomplete = 'select cm.id,m.name,cmc.userid,u.username,cm.instance,cmc.completionstate
					from mdl_modules m
					INNER join mdl_course_modules cm
					on m.id = cm.module
					INNER join mdl_course_modules_completion cmc
					on cmc.coursemoduleid = cm.id
					INNER JOIN mdl_user u
					on u.id = cmc.userid
					where cm.course = '.$data->courseid.' AND cm.instance = '.$mid->instance.' AND u.id = '.$userid->uid.'';
					$act = $DB->get_records_sql($acomplete);
					if(!empty($act)){
						$modids[$mid->id][]= $userid->uid.'-1';
					}else{
						$modids[$mid->id][]= $userid->uid.'-0';
					}
				}	
			}
		}
		
	}

}
$mform->display();
$table = new html_table();
$activity_header = array();
$activity_data = array();
$userids = array();
$thr = array();
$tmin = array();
$tsec = array();
$i = 1;
$count = 0;
$activity_header [] = 'Name';
$activity_header [] = 'Email';
foreach ($modids as $modid => $modval) {
	$crs_mod = $DB->get_record('course_modules',array('id'=>$modid),'module,instance');
	$inst = $DB->get_record('modules',array('id'=>$crs_mod->module),'name');
	$modobj = $DB->get_record($inst->name,array('id'=>$crs_mod->instance),'name');
	$modname = html_writer::tag('a', $modobj->name,array('href' => new moodle_url($CFG->wwwroot.'/mod/'.$inst->name.'/view.php?id='.$modid)));
	if($inst->name == 'scorm'){
		$count ++;
		$activity_header [] = $modname;
		$activity_header [] = $modobj->name.'<br>'.get_string('timespent','local_scormreport');
	}
	foreach ($modval as $uid) {
		$a = explode('-',$uid);
		$userids [$a[0]][] = $modid.'-'.$a[1];	
	}
	$i++;
}
$activity_header [] = 'Total Time';
if($count != 0){	
	$table->head = $activity_header;
	$temp = 0;
	foreach ($userids as $key => $value) {
		$uname = $DB->get_record('user',array('id'=>$key),'id,firstname,lastname,email');
		$username = '<strong>'.$uname->firstname.' '.$uname->lastname.'</strong>';
		$activity_data [] = html_writer::tag('a', $username,array('href' => new moodle_url($CFG->wwwroot.'/user/profile.php?id='.$uname->id)));;
		$activity_data [] = $uname->email;
		foreach ($value as $val) {
			$temp = $key;
			$a = explode('-',$val);
			$crs_mod = $DB->get_record('course_modules',array('id'=>$a[0]),'module,instance');
			$inst = $DB->get_record('modules',array('id'=>$crs_mod->module),'name');
			$modobj = $DB->get_record($inst->name,array('id'=>$crs_mod->instance),'name');
			if($inst->name == 'scorm'){
				$scoes = $DB->get_record('scorm_scoes_track',array('userid'=>$key,'scormid'=>$crs_mod->instance,'element'=>'cmi.core.lesson_status'),'value');
				$time = $DB->get_record('scorm_scoes_track',array('userid'=>$key,'scormid'=>$crs_mod->instance,'element'=>'cmi.core.total_time'),'value');
				if($scoes){
					$timesplit = explode(':',$time->value);
					$hr = $timesplit[0];
					$min = $timesplit[1];
					$sec = $timesplit[2];
					$timeon = $hr.' hours '.$min.' min '.$sec.' sec';
					$thr []= $hr;
					$tmin []= $min; 
					$tsec []= $sec;  
					if($scoes->value == 'completed'){
						$is_completed = get_string('completed','local_scormreport');
						$timespent = $timeon;
					}else if($scoes->value == 'incomplete'){
						$is_completed = get_string('notcompleted','local_scormreport');
						$timespent = $timeon;
					}
				}else{
					$is_completed = get_string('notstarted','local_scormreport');
					$timespent = get_string('na_crs','local_scormreport');
				}
				$activity_data [] = $is_completed;
				$activity_data [] = $timespent;
			}
		}
		$tthr = array_sum($thr);
		$ttmin = array_sum($tmin);
		$ttsec = array_sum($tsec);
		if($ttsec >= 60){
			$ttsec = $ttsec - 60;
			$ttmin = $ttmin + 1;
		}
		if($ttmin >= 60){
			$ttmin = $ttmin - 60;
			$tthr = $tthr + 1;
		}
		if($tthr < 10){
			$tthr = '0'.$tthr;
		}
		if($ttmin < 10){
			$ttmin = '0'.$ttmin;
		}
		if($ttsec < 10){
			$ttsec = '0'.$ttsec;
		}
		$activity_data [] = $tthr.' hr '.$ttmin.' min '.$ttsec.' sec';
		$thr = array();
		$tmin = array();
		$tsec = array();
		if($temp==$key){
			$samarray[$key] = $activity_data;
		}
		unset($temp);
		$activity_data = array();
	}
	foreach ($samarray as $key1 => $value1) {
		$table->data[] = $value1;
	}
}else{
	$table->data[] = array(
		'',
		'',
		'<p class="norecords">'.get_string('norecordfound', 'local_scormreport'),
		'',
		''
		);
}
echo html_writer::table($table);
echo $OUTPUT->footer();
?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.generaltable').DataTable( {
            dom: 'Bfrtip',
            buttons: [
            'excel', 'pdf', 'print'
            ]
        } );
    } );
</script>





