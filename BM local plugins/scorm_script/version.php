<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * 
 *
 * @package    local_scorm_script
 * @copyright   Dhruv Infoline Pvt Ltd   
 * @license     http://lmsofindia.com
 * @author     Prashant Yallatti <prashant@elearn10.com>
 * 
 */
defined('MOODLE_INTERNAL') || die();
$plugin->release    = '2.0.3 (Build: 2014040200)';
$plugin->version    = 2017080011;
$plugin->requires   = 2015102305; // Moodle 3.0 and above.
$plugin->component = 'local_scorm_script';
$plugin->maturity   = MATURITY_STABLE;
//$plugin->cron = 0;
