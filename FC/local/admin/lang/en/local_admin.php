<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * admin Language File
 *
 * @package    local_admin
 * @copyright  2016 Gerry G Hall
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$string['add_users'] = 'Add Users';
$string['pluginheading'] = 'Admin';
$string['pluginname'] = 'admin';
$string['user_management'] = 'Manage Users';
$string['user_team'] = 'Team';
$string['user_pass'] = 'Pass';
$string['virtual_certifcation'] = 'Virtual Certifcation';
$string['scheduling'] = 'Scheduling';
$string['marketing'] = 'Marketing';
$string['services'] = 'Services';
$string['reporting'] = 'Reporting';
$string['customimpact'] = 'Journey Builder';
$string['certifications'] = 'Certification';
$string['manage_libraries'] = 'Manage Libraries';
$string['add_users'] = 'Add Users';
$string['facilitator'] ='Facilitator';
$string['language'] = 'Language';
$string['last_updated'] = 'Last Updated';
$string['percentage_strings_translated'] = 'Percentage of language strings translated';
$string['user_teams'] = 'Team Management';
$string['role'] = 'Role';
$string['expires'] = 'Expires';
$string['diagnostic'] = 'Diagnostic';
$string['feedback'] = 'Feedback';
$string['content_library'] = 'Content Library';
$string['unassign'] = 'Unassign';
$string['create_new_team'] = 'Create new team';
$string['view_additional_info'] = 'View Additional Information';
$string['pagetitle-mynotifications'] = 'My Certifications';
$string['user_passes'] = 'Pass Management';
$string['deactivate'] = 'Deactivate';
$string['pagetitle-certifications'] = 'Certifications';
$string['useradded'] = 'User added success';
$string['pagetitle-services'] = 'Services & Materials';
$string['pagetitle-mynotifications'] = 'My Certifications';
$string['pagetitle-user_management'] = 'Manage users';
$string['customimpact-unpublished'] = 'Unpublished';
$string['customimpact-published'] = 'Published';
$string['pagetitle-index'] = 'Administration';
$string['pagetitle-user_team'] = 'Team Management';
$string['pagetitle-customimpact_published'] = 'Impact Journey Builder';
$string['pagetitle-customimpact_unpublished'] = 'Impact Journey Builder';
$string['pagetitle-standardpages'] = 'Standard pages';
$string['pagetitle-standardpages-tnc'] = 'Terms & Conditions';
$string['pagetitle-standardpages-policy'] = 'Policy';
$string['pagetitle-standardpages-faq'] = 'Help';
$string['pagetitle-reporting'] = 'Reports';
$string['pagetitle-scheduling'] = 'Scheduling';
$string['pagetitle-scheduling_active'] = 'Scheduled Worksessions';
$string['scheduling-active'] = 'Active';
$string['pagetitle-scheduling_completed'] = 'Scheduling Completed';
$string['scheduling-completed'] = 'Completed';

