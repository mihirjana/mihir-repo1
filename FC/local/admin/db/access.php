<?php
    // This file is part of Moodle - http://moodle.org/
    //
    // Moodle is free software: you can redistribute it and/or modify
    // it under the terms of the GNU General Public License as published by
    // the Free Software Foundation, either version 3 of the License, or
    // (at your option) any later version.
    //
    // Moodle is distributed in the hope that it will be useful,
    // but WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    // GNU General Public License for more details.
    //
    // You should have received a copy of the GNU General Public License
    // along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

    /**
     * Latest badges block capabilities.
     * @package    local_admin
     * @copyright  2017 Gerry G Hall
     * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
     */
    defined('MOODLE_INTERNAL') || die;
    $capabilities = [
        'moodle/local_admin:managementblock' => [
            'captype' => 'read', 'contextlevel' => CONTEXT_SYSTEM, 'archetypes' => [
                'manager' => CAP_ALLOW
            ],
        ],

        'moodle/local_admin:viewcertifications' => [
            'captype' => 'read', 'contextlevel' => CONTEXT_SYSTEM, 'archetypes' => [
                'manager' => CAP_ALLOW
            ],
        ],

        'moodle/local_admin:schedulingblock' => [
            'captype' => 'read', 'contextlevel' => CONTEXT_SYSTEM, 'archetypes' => [
                'manager' => CAP_ALLOW
            ],
        ],

        'moodle/local_admin:marketingblock' => [
            'captype' => 'read', 'contextlevel' => CONTEXT_SYSTEM, 'archetypes' => [
                'manager' => CAP_ALLOW
            ],
        ],

        'moodle/local_admin:servicesblock' => [
            'captype' => 'read', 'contextlevel' => CONTEXT_SYSTEM, 'archetypes' => [
                'manager' => CAP_ALLOW
            ],
        ],

        'moodle/local_admin:reportingblock' => [
            'captype' => 'read', 'contextlevel' => CONTEXT_SYSTEM, 'archetypes' => [
                'manager' => CAP_ALLOW
            ],
        ],

        'moodle/local_admin:customimpactjourneyblock' => [
            'captype' => 'read', 'contextlevel' => CONTEXT_SYSTEM, 'archetypes' => [
                'manager' => CAP_ALLOW
            ],
        ],
		
		'moodle/local_admin:standardpages' => [
            'captype' => 'read', 'contextlevel' => CONTEXT_SYSTEM, 'archetypes' => [
                'manager' => CAP_ALLOW
            ],
        ],

        'moodle/local_admin:facilitatorselect' => [
            'captype' => 'read', 'contextlevel' => CONTEXT_SYSTEM, 'archetypes' => [
                'manager' => CAP_ALLOW
            ],
        ], 'moodle/local_admin:admintab'       => [
            'captype' => 'read', 'contextlevel' => CONTEXT_SYSTEM, 'archetypes' => [
                'manager' => CAP_ALLOW
            ],
        ],
    ];