<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Admin renderers.
 *
 * @package    local_admin
 * @copyright  2016 Gerry G HAll
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use local_admin\local;

defined('MOODLE_INTERNAL') || die;

class local_admin_renderer extends plugin_renderer_base {

    public function print_admin_boxes () {

        $results = new \local_admin\local\adminblocks(\context_system::instance());
        $data = $results->export_for_template($this);
        echo parent::render_from_template('local_admin/adminblocks', $data);
    }

    public function print_page( $type ) {
        switch ( $type ) {
            case 'user_management' :
                $mange = new \adminpages_user\management($this->page);
                echo $mange->top_banner();
                echo $mange->body($this);
                break;
            case 'user_team' :
                $mange = new \adminpages_user\teams($this->page);
                echo $mange->top_banner();
                echo $mange->body($this);
                break;
            case 'user_pass' :
                $mange = new \adminpages_user\passes($this->page);
                $mange->init();
                echo $mange->top_banner();
                echo $mange->body($this);
                break;
            case 'reporting' :
                $mange = new \adminpages_reporting\reporting($this->page);
                echo $mange->top_banner();
                echo $mange->body($this);
                break;
            case 'certifications' :
                $mange = new \adminpages_certifications\certifications($this->page);
                echo $mange->top_banner();
                echo $mange->body($this);
                break;
            case 'marketing' :
                $mange = new \adminpages_marketing\marketing($this->page);
                echo $mange->top_banner();
                echo $mange->body($this);
                break;
            case 'services' :
                $mange = new \adminpages_services\services($this->page);
                echo $mange->top_banner();
                echo $mange->body($this);
                break;
            case 'customimpact_published' :
                $mange = new \adminpages_customimpact\published($this->page);
                echo $mange->top_banner();
                echo $mange->body($this);
                break;
            case 'customimpact_unpublished' :
                $mange = new \adminpages_customimpact\unpublished($this->page);
                echo $mange->top_banner();
                echo $mange->body($this);
                break;
            case 'scheduling_active' :
                $mange = new \adminpages_scheduling\active($this->page);
                echo $mange->top_banner();
                echo $mange->body($this);
                break;
            case 'scheduling_completed' :
                $mange = new \adminpages_scheduling\completed($this->page);
                echo $mange->top_banner();
                echo $mange->body($this);
                break;
            case 'standardpages' :
                $lang = optional_param('lang', false, PARAM_RAW);
                $section = optional_param('section', false, PARAM_RAW);
                if($lang && $section) {
                    $mange = new \adminpages_standardpages\standardpages(
                        $this->page
                    );
                    echo $mange->content(
                        $this,
                        required_param('lang', PARAM_RAW),
                        required_param('section', PARAM_RAW)
                    );
                } else {
                    // Main Page
                    $mange = new \adminpages_standardpages\standardpages($this->page);
                    echo $mange->body($this);
                }
                break;
            case 'index' :
            default:
                $this->print_admin_boxes();
                break;
        }

    }

}
