<?php
// this file is part of moodle - http://moodle.org/
//
// moodle is free software: you can redistribute it and/or modify
// it under the terms of the gnu general public license as published by
// the free software foundation, either version 3 of the license, or
// (at your option) any later version.
//
// moodle is distributed in the hope that it will be useful,
// but without any warranty; without even the implied warranty of
// merchantability or fitness for a particular purpose.  see the
// gnu general public license for more details.
//
// you should have received a copy of the gnu general public license
// along with moodle.  if not, see <http://www.gnu.org/licenses/>.


/**
 * 
 * @package     local_admin
 * @subpackage  
 * @copyright   2017 gerry g hall (gerryghall.co.uk)
 * @license     http://www.gnu.org/copyleft/gpl.html gnu gpl v3 or later
 */

    class pagination
    {
        /**
         * params
         *
         * sets default variables for the rendering of the pagination markup.
         *
         * @var    array
         * @access protected
         */
        protected $params = array(
            'class' => 'clearfix pagination',
            'crumbs' => 5,
            'rpp' => 10,
            'key' => 'page',
            'target' => '',
            'next' => 'next &raquo;',
            'previous' => '&laquo; previous',
            'show' => false,
            'clean' => false
        );

        /**
         * __construct
         *
         * @access public
         * @param  integer $current (default: null)
         * @param  integer $total (default: null)
         * @return void
         */
        public function __construct($current = null, $total = null)
        {
            // current instantiation setting
            if (!is_null($current)) {
                $this->set_current($current);
            }

            // total instantiation setting
            if (!is_null($total)) {
                $this->set_total($total);
            }

            // pass along get (for link generation)
            $this->params['get'] = $_get;
        }

        /**
         * _check
         *
         * checks the current (page) and total (records) parameters to ensure
         * they've been set. throws an exception otherwise.
         *
         * @access protected
         * @return void
         */
        protected function _check()
        {
            if (!isset($this->params['current'])) {
                throw new exception('pagination::current must be set.');
            } elseif (!isset($this->params['total'])) {
                throw new exception('pagination::total must be set.');
            }
        }

        /**
         * style
         *
         * sets the classes to be added to the pagination div node.
         * useful with twitter bootstrap (eg. pagination-centered, etc.)
         *
         * @see    <http://twitter.github.com/bootstrap/components.html#pagination>
         * @access public
         * @param  mixed $classes
         * @return void
         */
        public function style($classes)
        {
            $this->params['class'] = $this->params['class'] . " " . $classes;
        }

        /**
         * show
         *
         * tells the rendering engine to show the pagination links even if there
         * aren't any pages to paginate through.
         *
         * @access public
         * @return void
         */
        public function show()
        {
            $this->params['show'] = true;
        }

        /**
         * getcanonicalurl
         *
         * @access public
         * @return string
         */
        public function getcanonicalurl()
        {
            $target = $this->params['target'];
            if (empty($target)) {
                $target = $_server['php_self'];
            }
            $page = (int) $this->params['current'];
            if ($page !== 1) {
                return 'http://' . ($_server['http_host']) . ($target) . $this->get_page();
            }
            return 'http://' . ($_server['http_host']) . ($target);
        }

        /**
         * get_page
         *
         * @access public
         * @param  boolean|integer $page (default: false)
         * @return string
         */
    public function get_page($page = false)
        {
            if ($page === false) {
                $page = (int) $this->params['current'];
            }
            $key = $this->params['key'];
            return '?' . ($key) . '=' . ((int) $page);
        }

        /**
         * getpageurl
         *
         * @access public
         * @param  boolean|integer $page (default: false)
         * @return string
         */
        public function getpageurl($page = false)
        {
            $target = $this->params['target'];
            if (empty($target)) {
                $target = $_server['php_self'];
            }
            return 'http://' . ($_server['http_host']) . ($target) . ($this->get_page($page));
        }

        /**
         * getrelprevnextlinktags
         *
         * @see    http://support.google.com/webmasters/bin/answer.py?hl=en&answer=1663744
         * @see    http://googlewebmastercentral.blogspot.ca/2011/09/pagination-with-relnext-and-relprev.html
         * @see    http://support.google.com/webmasters/bin/answer.py?hl=en&answer=139394
         * @access public
         * @return array
         */
        public function getrelprevnextlinktags()
        {
            // generate path
            $target = $this->params['target'];
            if (empty($target)) {
                $target = $_server['php_self'];
            }
            $key = $this->params['key'];
            $params = $this->params['get'];
            $params[$key] = 'pgnmbr';
            $href = ($target) . '?' . http_build_query($params);
            $href = preg_replace(
                array('/=$/', '/=&/'),
                array('', '&'),
                $href
            );
            $href = 'http://' . ($_server['http_host']) . $href;

            // pages
            $currentpage = (int) $this->params['current'];
            $numberofpages = (
            (int) ceil(
                $this->params['total'] /
                $this->params['rpp']
            )
            );

            // on first page
            if ($currentpage === 1) {

                // there is a page after this one
                if ($numberofpages > 1) {
                    $href = str_replace('pgnmbr', 2, $href);
                    return array(
                        '<link rel="next" href="' . ($href) . '" />'
                    );
                }
                return array();
            }

            // store em
            $prevnexttags = array(
                '<link rel="prev" href="' . (str_replace('pgnmbr', $currentpage - 1, $href)) . '" />'
            );

            // there is a page after this one
            if ($numberofpages > $currentpage) {
                array_push(
                    $prevnexttags,
                    '<link rel="next" href="' . (str_replace('pgnmbr', $currentpage + 1, $href)) . '" />'
                );
            }
            return $prevnexttags;
        }

        /**
         * parse
         *
         * parses the pagination markup based on the parameters set and the
         * logic found in the render.inc.php file.
         *
         * @access public
         * @return void
         */
        public function parse()
        {
            // ensure required parameters were set
            $this->_check();

            // bring variables forward
            foreach ($this->params as $_name => $_value) {
                $$_name = $_value;
            }

            // buffer handling
            ob_start();
            include 'render.inc.php';
            $_response = ob_get_contents();
            ob_end_clean();
            return $_response;
        }

        /**
         * set_style
         *
         * @see    <http://twitter.github.com/bootstrap/components.html#pagination>
         * @access public
         * @param  sting $classes
         * @return void
         */
        public function set_style($classes)
        {
            $this->params['classes'] = $classes;
        }

        /**
         * set_clean
         *
         * sets the pagination to exclude page numbers, and only output
         * previous/next markup. the counter-method of this is self::set_full.
         *
         * @access public
         * @return void
         */
        public function set_clean()
        {
            $this->params['clean'] = true;
        }

        /**
         * set_crumbs
         *
         * sets the maximum number of 'crumbs' (eg. numerical page items)
         * available.
         *
         * @access public
         * @param  integer $crumbs
         * @return void
         */
        public function set_crumbs($crumbs)
        {
            $this->params['crumbs'] = $crumbs;
        }

        /**
         * set_current
         *
         * sets the current page being viewed.
         *
         * @access public
         * @param  integer $current
         * @return void
         */
        public function set_current($current)
        {
            $this->params['current'] = (int)$current;
        }

        /**
         * set_full
         *
         * see self::set_clean for documentation.
         *
         * @access public
         * @return void
         */
        public function set_full()
        {
            $this->params['clean'] = false;
        }

        /**
         * set_key
         *
         * sets the key of the <_get> array that contains, and ought to contain,
         * paging information (eg. which page is being viewed).
         *
         * @access public
         * @param  string $key
         * @return void
         */
        public function set_key($key)
        {
            $this->params['key'] = $key;
        }

        /**
         * set_next
         *
         * sets the copy of the next anchor.
         *
         * @access public
         * @param  string $str
         * @return void
         */
        public function set_next($str)
        {
            $this->params['next'] = $str;
        }

        /**
         * set_previous
         *
         * sets the copy of the previous anchor.
         *
         * @access public
         * @param  string $str
         * @return void
         */
        public function set_previous($str)
        {
            $this->params['previous'] = $str;
        }

        /**
         * set_rpp
         *
         * sets the number of records per page (used for determining total
         * number of pages).
         *
         * @access public
         * @param  integer $rpp
         * @return void
         */
        public function set_rpp($rpp)
        {
            $this->params['rpp'] = (int)$rpp;
        }

        /**
         * set_target
         *
         * sets the leading path for anchors.
         *
         * @access public
         * @param  string $target
         * @return void
         */
        public function set_target($target)
        {
            $this->params['target'] = $target;
        }

        /**
         * set_total
         *
         * sets the total number of records available for pagination.
         *
         * @access public
         * @param  integer $total
         * @return void
         */
        public function set_total($total)
        {
            $this->params['total'] = (int)$total;
        }
    }