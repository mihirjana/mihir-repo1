<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Bookmark index.
 *
 * @package    local_admin
 * @copyright  2016 Gerry G Hall
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');

$heading = get_string('pluginheading', 'local_admin');
$page = optional_param('page', 'index', PARAM_ALPHAEXT);
$section = optional_param('section', '', PARAM_ALPHAEXT);

$PAGE->set_context(context_system::instance());
$url = new moodle_url('/local/admin/index.php');

if ($page != 'index') {
    $url->param('page',$page);
}

$pagetitle = 'pagetitle-' . $page;

if(!empty($section)) {
    $pagetitle .= '-'.$section;
}

$PAGE->set_subpage(get_string($pagetitle, 'local_admin'));
$PAGE->set_url($url);
$PAGE->set_pagetype('admin-' . $page);
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string($pagetitle, 'local_admin'));
$renderer = $PAGE->get_renderer('local_admin');

echo $OUTPUT->header();


$renderer->print_page($page);

echo $OUTPUT->footer();

