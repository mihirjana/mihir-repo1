<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @package    local_admin
 * @author     Gerry G Hall
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_admin;


defined('MOODLE_INTERNAL') || die();

class adminpage {

    protected $page;

    public function __construct(\moodle_page $page ) {
        $this->page = $page;
    }

    public function top_banner() {
        return '';
    }

    public function body(\renderer_base $renderer) {
        return '';
    }
    public function tabs() {
        return '';
    }


}