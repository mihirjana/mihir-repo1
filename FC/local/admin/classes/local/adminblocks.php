<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_admin\local;

class adminblocks implements \renderable, \templatable {

    public $blocks;

    public function __construct ($context) {
       $this->get_user_caps($context);

    }
    protected function get_user_caps (\context $context) {
        global $OUTPUT, $PAGE;

        $adminblocks = array();
        if (has_capability('moodle/local_admin:managementblock', $context)) {

            $data = new \stdClass();
            $data->imageurl = $OUTPUT->pix_url('management', 'theme');
            $data->description = "";
            $data->url = new \moodle_url('/local/admin/index.php');
            $data->url->param('page', 'user_management');
            $data->overtext = get_string('user_management', 'local_admin');
            $adminblocks[] = new adminblock($data);
        }
        if (has_capability('moodle/local_admin:schedulingblock', $context)) {
            $data = new \stdClass();
            $data->imageurl = $OUTPUT->pix_url('scheduling', 'theme');
            $data->description = "";
            $data->url = new \moodle_url('/local/admin/index.php');
            $data->url->param('page', 'scheduling_active');
            $data->overtext = get_string('scheduling', 'local_admin');
            $adminblocks[] = new adminblock($data);
        }
        if (has_capability('moodle/local_admin:marketingblock', $context)) {
            $data = new \stdClass();
            $data->imageurl = $OUTPUT->pix_url('marketing', 'theme');
            $data->description = "";
            $data->url = new \moodle_url('/local/admin/index.php');
            $data->url->param('page', 'marketing');
            $data->overtext = get_string('marketing', 'local_admin');
            $adminblocks[] = new adminblock($data);
        }
        if (has_capability('moodle/local_admin:servicesblock', $context)) {
            $data = new \stdClass();
            $data->imageurl = $OUTPUT->pix_url('services', 'theme');
            $data->description = "";
            $data->url = new \moodle_url('/local/admin/index.php');
            $data->url->param('page', 'services');
            $data->overtext = get_string('services', 'local_admin');
            $adminblocks[] = new adminblock($data);
        }
        if (has_capability('report/log:view', $context)) {
            $data = new \stdClass();
            $data->imageurl = $OUTPUT->pix_url('reporting', 'theme');
            $data->description = "";
            $data->url = new \moodle_url('/local/admin/index.php');
            $data->url->param('page', 'reporting');
            $data->overtext = get_string('reporting', 'local_admin');
            $adminblocks[] = new adminblock($data);
        }
        if (has_capability('moodle/local_admin:customimpactjourneyblock', $context)) {
            $data = new \stdClass();
            $data->imageurl = $OUTPUT->pix_url('customimpact', 'theme');
            $data->description = "";
            $data->url = new \moodle_url('/local/admin/index.php');
            $data->url->param('page', 'customimpact_unpublished');
            $data->overtext = get_string('customimpact', 'local_admin');
            $adminblocks[] = new adminblock($data);
        }
        if (has_capability('moodle/local_admin:viewcertifications', $context)) {
            $data = new \stdClass();
            $data->imageurl = $OUTPUT->pix_url('certification', 'theme');
            $data->description = "";
            $data->url = new \moodle_url('/local/admin/index.php');
            $data->url->param('page', 'certifications');
            $data->overtext = get_string('certifications', 'local_admin');
            $adminblocks[] = new adminblock($data);

        }
		if (has_capability('moodle/local_admin:standardpages', $context)) {
            $data = new \stdClass();
            $data->imageurl = $OUTPUT->pix_url('standardpages', 'theme');
            $data->description = "";
            $data->url = new \moodle_url('/local/admin/index.php');
            $data->url->param('page', 'standardpages');
            $data->overtext = get_string('standardpages', 'adminpages_standardpages');
            $adminblocks[] = new adminblock($data);

        }
        $this->blocks = $adminblocks;
    }

    public function  export_for_template(\renderer_base $output) {
        $data = new \stdClass();
        $data->blocks = $this->blocks;
        return $data;
    }
}
