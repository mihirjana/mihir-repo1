<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   theme_franklincovey
 * @copyright 2016 Gerry G Hall
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_admin\local;

class block implements \renderable, \templatable {
    const BASEUSR = '/local/admin/index.php';
    public $imageurl = "";
    public $description = "";
    public $overtext = "";
    public $url = '';

    public function __construct(\stdClass $record) {
        $this->imageurl = $record->imageurl;
        $this->description = $record->description;
        $this->overtext = $record->overtext;
        $this->url = $record->url;

    }
    public function export_for_template(\renderer_base $output) {
        global $CFG;

        $data = new \stdClass();
        $data->imageurl = $this->imageurl;
        $data->description = $this->description;
        $data->url = $this->url->out();
        $data->overtext = format_string($this->overtext);
        return $data;
    }

}