<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   theme_franklincovey
 * @copyright 2016 Gerry G Hall
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_admin\local;

class adminblock extends block implements \renderable, \templatable {

    public function __construct(\stdClass $record) {
       parent::__construct($record);
    }

    public static function get_tabs($type) {
        global $PAGE;

        $tabitems = [];
        switch ($type) {
            case 'user_management' :
            case 'user_team' :
            case 'user_pass' :

            $tabitems[] = new \tabobject('user_management',
                new \moodle_url(self::BASEUSR, ['page'=> 'management']),
                get_string('user_management', 'local_admin'));
            $tabitems[] = new \tabobject('user_team',
                new \moodle_url(self::BASEUSR, ['page'=> 'user_team']),
                get_string('user_team', 'local_admin'));
            $tabitems[] = new \tabobject('user_pass',
                new \moodle_url(self::BASEUSR, ['page'=> 'user_pass']),
                get_string('user_pass', 'local_admin'));
            break;
            case 'customimpact_unpublished' :
            case 'customimpact_published' :
                $tabitems[] = new \tabobject('customimpact_unpublished',
                    new \moodle_url(self::BASEUSR, ['page'=> 'customimpact_unpublished']),
                    get_string('customimpact-unpublished', 'local_admin'));
                $tabitems[] = new \tabobject('customimpact_published',
                    new \moodle_url(self::BASEUSR, ['page'=> 'customimpact_published']),
                    get_string('customimpact-published', 'local_admin'));
                break;

            case 'scheduling_active' :
            case 'scheduling_completed' :
                $tabitems[] = new \tabobject('scheduling_active',
                    new \moodle_url(self::BASEUSR, ['page'=> 'scheduling_active']),
                    get_string('scheduling-active', 'local_admin'));
                $tabitems[] = new \tabobject('scheduling_completed',
                    new \moodle_url(self::BASEUSR, ['page'=> 'scheduling_completed']),
                    get_string('scheduling-completed', 'local_admin'));
                break;
        }
        return $tabitems;
    }

}