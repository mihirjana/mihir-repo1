<?php

function get_admin_standardpages($shortname, $lang = 'en')
{
    global $DB;
    return $DB->get_record('adminpages_standard', array('lang' => $lang, 'shortname' => $shortname));
}

function get_all_admin_standardpages() {
    global $DB;
    return $DB->get_records('adminpages_standard');
}

function  get_standardpage_url($shortname) {
    global $CFG, $USER;

    if(isloggedin()) {
        return new moodle_url(
            '/local/admin/index.php',
            [
                'page' => 'standardpages',
                'section' => $shortname,
                'lang' => $USER->lang
            ]
        );
    }
 
    return new moodle_url('/local/admin/index.php',[
        'page' => 'standardpages',
        'section' => $shortname,
        'lang' => 'en'
    ]); 
}

function manage_standardpages($lang = 'en', $shortname, $content ='', $format = 1) {
    global $DB;
    $adminpage = $DB->get_record('adminpages_standard', array('lang' => $lang, 'shortname' => $shortname));
    if($adminpage) {
        $adminpage->content = $content;
        $adminpage->contentformat = $format;
        $DB->update_record('adminpages_standard', $adminpage);
        return true;
    }

    $DB->insert_record(
        'adminpages_standard',
        [
            'shortname' => $shortname,
            'lang' => $lang,
            'content' => $content,
            'contentformat' => $format
        ]
    );
}
