<?php

// This file is part of Digital Library Plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require ('../../../../config.php');

require_login(SITEID, false);

$id = required_param('id', PARAM_INT);
$delete = optional_param('delete', '', PARAM_ALPHANUM); // Confirmation hash.

try {
    $standardpage = $DB->get_record('adminpages_standard', array('id' => $id), '*', MUST_EXIST);

} catch (dml_missing_record_exception $e) {
    throw new moodle_exception('you do not have permission to delete this standardpage');
    $standardpage = null;
}

$url = new moodle_url($CFG->wwwroot . '/local/admin/index.php', array('page' => 'standardpages'));
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title(get_string('deletecontent', 'adminpages_standardpages'));
$PAGE->set_heading(get_string('deletecontent', 'adminpages_standardpages'));
$PAGE->set_url($url);
$PAGE->navbar->ignore_active();
$PAGE->navbar->add(get_string('pluginname', 'adminpages_standardpages'));
$PAGE->navbar->add(get_string('confirmdelete', 'adminpages_standardpages', $standardpage));
$continueurl = new moodle_url('/local/admin/pages/standardpages/delete.php', array('id' => $standardpage->id, 'delete' => md5($standardpage->lang)));
$continuebutton = new single_button($continueurl, get_string('delete'), 'post');

// Check if we've got confirmation.
if ($delete === md5($standardpage->lang)) {
    // We do - time to delete the course.
    require_sesskey();
    echo $OUTPUT->header();
    $DB->delete_records('adminpages_standard', array('id' => $standardpage->id));
    echo html_writer::tag('p', get_string("deletedcourse", "", $standardpage->lang), array('class' => 'lead'));
    echo $OUTPUT->continue_button($url);
    echo $OUTPUT->footer();
    exit; // We must exit here!!!
}

echo $OUTPUT->header();
echo $OUTPUT->confirm(get_string('confirm', 'adminpages_standardpages', $standardpage), $continuebutton, $url);
echo $OUTPUT->footer();
