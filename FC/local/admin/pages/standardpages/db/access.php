<?php

defined('MOODLE_INTERNAL') || die;

$capabilities = array(
    'moodle/adminpages_standardpages:edit' => array(
        'captype' => 'read',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
            'manager' => CAP_ALLOW
        )
    )
);
