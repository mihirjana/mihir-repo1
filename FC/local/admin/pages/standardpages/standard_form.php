<?php

class standard_form extends moodleform {
    //Add elements to form
    public function definition() {
        global $CFG;

        $mform = $this->_form;
        $label = (optional_param('id', -1, PARAM_INT) == -1) ? 'addnew' : 'edit';
        $mform->addElement('header', 'addreport', get_string($label, 'adminpages_standardpages'));
	    
        $mform->addElement('hidden', 'id');
	    $mform->setDefault('id', $this->_customdata['id']);
        $mform->setType('id',PARAM_INT);
        
        $mform->addElement('select', 'lang', get_string('preferredlanguage'), get_string_manager()->get_list_of_translations());
        $mform->setType('lang', PARAM_NOTAGS);

        $mform->addElement('text', 'shortname', get_string('shortname', 'adminpages_standardpages'));
        $mform->setType('shortname', PARAM_NOTAGS);
        $mform->addRule('shortname', get_string('err_shortname', 'adminpages_standardpages'), 'required', null, 'server', false, false);

        $mform->addElement('editor', 'content', get_string('content', 'adminpages_standardpages'));
        $mform->addRule('content', get_string('err_content', 'adminpages_standardpages'), 'required', null, 'server', false, false);
        
        $mform->addElement('hidden', 'contentformat', 1);
        $mform->setType('contentformat', PARAM_INT);
        
        $this->add_action_buttons();
    }
    //Custom validation should be added here
    function validation($data, $files) {
        global $DB;

        if($data['id'] == -1) {
            $langflag = $DB->record_exists('adminpages_standard', array('lang' => $data['lang'], 'shortname' => $data['shortname']));
            if($langflag) {
                return [
                    'lang' => get_string('langexist', 'adminpages_standardpages'),
                    'shortname' => get_string('shortnameexist', 'adminpages_standardpages')
                ];
            }
        } else {
            $lang = $DB->get_record('adminpages_standard', array('lang' => $data['lang'], 'shortname' => $data['shortname']));
            if($lang) {
                if($lang->id != $data['id']) {
                    return [
                        'lang' => get_string('langexist', 'adminpages_standardpages'),
                        'shortname' => get_string('shortnameexist', 'adminpages_standardpages')
                    ];
                }
            }
        }
        return [];
    }
}
