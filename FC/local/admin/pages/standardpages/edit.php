<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * standardpages plugin
 *
 * @package    adminpages
 * @subpackage standardpages
 */
require ('../../../../config.php');
require_once ($CFG->libdir.'/formslib.php');
require ('standard_form.php');

require_login(0, false);

$id = optional_param('id', -1, PARAM_INT);
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_url(new moodle_url('/local/admin/pages/standardpages/edit.php', array('id' => $id)));
$form = new standard_form(null, array('id' => $id));

if ($id == -1) {
    $PAGE->set_title(get_string('addnew', 'adminpages_standardpages'));
    $PAGE->set_heading(get_string('addnew', 'adminpages_standardpages'));
    $PAGE->navbar->add(get_string('pluginname', 'adminpages_standardpages'), new moodle_url($CFG->wwwroot.'/local/admin/index.php', ['page' => 'standardpages']));
    $PAGE->navbar->add(get_string('addnew', 'adminpages_standardpages'));
} else {
    $PAGE->set_title(get_string('edit', 'adminpages_standardpages'));
    $PAGE->set_heading(get_string('edit', 'adminpages_standardpages'));
    $PAGE->navbar->add(get_string('pluginname', 'adminpages_standardpages'), new moodle_url($CFG->wwwroot.'/local/admin/index.php', ['page' => 'standardpages']));
    $PAGE->navbar->add(get_string('edit', 'adminpages_standardpages'));
    
    $standardpages = $DB->get_record('adminpages_standard', array('id' => $id), '*', MUST_EXIST);
    if ($standardpages) {
        $existdata = new stdClass();
        $existdata->id = $standardpages->id;
        $existdata->lang = $standardpages->lang;
        $existdata->shortname = $standardpages->shortname;
        $existdata->content['text'] = $standardpages->content;
        $form->set_data($existdata);
    }
}

$redirecturl = new moodle_url($CFG->wwwroot . '/local/admin/index.php', ['page' => 'standardpages']);
if ($form->is_cancelled()) {
    redirect($redirecturl);
} else if ($data = $form->get_data()) {
    if ($data->id == -1) {
        // add new standardpages
        $data->content = $data->content['text'];
        $lastinsert = $DB->insert_record('adminpages_standard', $data);
        if ($lastinsert) {
            redirect($redirecturl, html_writer::div(get_string('contentadded', 'adminpages_standardpages')));
        }
    } else {
        // update standardpages
        $data->content = $data->content['text'];
        if ($DB->update_record('adminpages_standard', $data)) {
            redirect($redirecturl, get_string('contentupdated', 'adminpages_standardpages'));
        }
    }
}
echo $OUTPUT->header();
echo $form->display();
echo $OUTPUT->footer();
