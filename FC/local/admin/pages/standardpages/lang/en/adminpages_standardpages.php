<?php
/*
 * Language strings
 *
 */
$string['pluginname'] = 'Standardpages';
$string['plugin'] = 'Standardpages';
$string['title'] = 'Policy & T&C Pages';
$string['heading'] = 'Policy & T&C Pages';
$string['action'] = 'Action';
$string['edit'] = 'Edit';
$string['delete'] = 'Delete';
$string['addnew'] = 'Add new content';
$string['policyname'] = 'Policy name';
$string['policyshrtname'] = 'Policy shortname';
$string['policycontent'] = 'Policy content';
$string['tncname'] = 'T&C Name';
$string['tncshortname'] = 'T&C Shortname';
$string['tnccontent'] = 'T&C Content';
$string['lang'] = 'Language';
$string['sl'] = '#';
$string['langexist'] = 'Language exist';
$string['contentadded'] = 'Content added successfully';
$string['contentupdated'] = 'Content updated successfully';
$string['deletecontent'] = 'Content delete';
$string['deleted'] = 'Content deleted successfully';
$string['confirm'] = 'Confirm';
$string['view'] = 'View';
$string['nocontent'] = 'No content exists';
$string['confirmdelete'] = 'Confirm delete';
$string['standardpages'] = 'Standard Pages';
$string['page'] = 'Page';
$string['err_content'] = 'Content';
$string['content'] = 'Content';
$string['err_shortname'] = 'Shortname';
$string['shortname'] = 'Shortname';
$string['shortnameexist'] = 'Shortname already exist';
