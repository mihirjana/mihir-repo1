<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Core Report class of basic reporting plugin
 * @package    scormreport
 * @subpackage basic
 * @author     Dan Marsden and Ankit Kumar Agarwal
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace adminpages_standardpages;

defined('MOODLE_INTERNAL') || die();

include_once (dirname(dirname(__FILE__)).'/lib.php');

use moodle_url;


class standardpages extends \local_admin\adminpage {

    static $type = 'services';
    public $total;
    public $users;

    public function __construct(\moodle_page $page) {
        parent::__construct($page);
    }

    public function body(\renderer_base $renderer) {

        $adminpages =  get_all_admin_standardpages();
        $data= [];

        foreach($adminpages as $page) {
            $data[] = [
                'lang' => $page->lang,
                'shortname' => $page->shortname,
                'editurl' => new moodle_url(
                    '/local/admin/pages/standardpages/edit.php',
                    array(
                        'id' => $page->id
                    )
                ),
                'deleteurl' => new moodle_url(
                    '/local/admin/pages/standardpages/delete.php',
                    array(
                        'id' => $page->id
                    )
                ),
                'viewurl' => new moodle_url(
                    '/local/admin/index.php',
                    array(
                        'page' => 'standardpages',
                        'lang' => $page->lang,
                        'section' => $page->shortname
                    )
                )
            ];
        }
        $addnew = new moodle_url('/local/admin/pages/standardpages/edit.php', array('id' => -1));
        return $renderer->render_from_template('adminpages_standardpages/body', ['adminpages' => $data, 'addnew' => $addnew]);
    }

    public function content($renderer, $lang, $shortname) {
        $content = get_admin_standardpages($shortname, $lang);
        if($content) {
            $content = $content->content;
        } 
        return $renderer->render_from_template('adminpages_standardpages/content', compact('content'));
    }
}
