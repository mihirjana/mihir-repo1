<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Core Report class of basic reporting plugin
 * @package    scormreport
 * @subpackage basic
 * @author     Dan Marsden and Ankit Kumar Agarwal
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace adminpages_user;

defined('MOODLE_INTERNAL') || die();


class management extends \local_admin\adminpage {

    static $type = 'user_management';
    public $total;
    public $users;

    public function __construct(\moodle_page $page) {
        parent::__construct($page);
        $this->total = users::get_user_count();
    }

    public function body(\renderer_base $renderer) {
        global $CFG;
        $this->page->requires->js_call_amd('adminpages_user/faci-role', 'init'); // FC35 elighten

        $out = '';
        $pagination = new \theme_franklincovey\local\pagination($this->page->url);
        $pagination->set_total($this->total);
        $pagination->set_rpp(optional_param('rpp', 5, PARAM_INT));
        $pagination->set_current(optional_param('position', 1, PARAM_INT));

        $users = new users();
        $userdata = $users->load_users($pagination->pointer(), $pagination->rpp())->export_for_template($renderer);
        $out .= $renderer->render_from_template('adminpages_user/user_table', $userdata);
        $pagdata = $pagination->export_for_template($renderer);
        $out .= $renderer->render_from_template('theme_franklincovey/pagination', $pagdata);
        return $out;
    }


    // Header
    public function top_banner() {
        // Title
        $banner = \html_writer::start_div('content-header');
        $banner .= \html_writer::tag('h3', get_string( self::$type, 'local_admin'), array('class' => 'title'));
        // Amount
        $banner .= \html_writer::start_div('amount');
        $banner .= \html_writer::span($this->total . '<small>Arbitrary Stat #</small>');
        $banner .= \html_writer::span(94 . '<small>Users</small>');
        $banner .= \html_writer::end_div();
        $banner .= \html_writer::end_div();

        // Filter - items per page && search
        $banner .= \html_writer::start_div('table-filter clearfix');
        // Search
        $banner .= \html_writer::start_div('search-field');
        $banner .= \html_writer::tag('input', null, array('class'=>'input-search'));
        $banner .= \html_writer::end_div();
        // Items per page (<select>)
        $banner .= \html_writer::label('Per Page:', 'qnt_per_page');
        $banner .= \html_writer::select([10,20,30], 'qnt_per_page', '30', null, array('class'=>'select-filter', 'placeholder' => 'Search For Users'));
        
        $banner .= \html_writer::end_div();

        return $banner;
    }

}
