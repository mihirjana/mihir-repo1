<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Core Report class of basic reporting plugin
 * @package    scormreport
 * @subpackage basic
 * @author     Dan Marsden and Ankit Kumar Agarwal
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace adminpages_user;

defined('MOODLE_INTERNAL') || die();


class passes extends \local_admin\adminpage {

    static $type = 'user_passes';
    public $data;
    public $seatsremaining = 0;
    public $total;


    public function __construct(\moodle_page $page) {
        parent::__construct($page);
    }

    public function body(\renderer_base $renderer) {
        global $DB, $USER;
        $out = '';
        $this->page->requires->js_call_amd('theme_franklincovey/add-userspass', 'init');
        $this->page->requires->js_call_amd('theme_franklincovey/pass-usersearch', 'init');
        $this->page->requires->js_call_amd('theme_franklincovey/cohort-switch', 'init');
        $pagination = new \theme_franklincovey\local\pagination($this->page->url);
        $this->page->requires->js_call_amd('theme_franklincovey/pass-usersearch', 'init');
        $pagination->set_total($this->total);
        $pagination->set_rpp(optional_param('rpp', 5, PARAM_INT));
        $pagination->set_current(optional_param('position', 1, PARAM_INT));

        $users = new users();
        $userdata = $users->load_users($pagination->pointer(), $pagination->rpp())->export_for_template($renderer);
        $usercontext = \context_user::instance($USER->id);
        $capability = false;
        if(has_capability('moodle/theme_franklincovey:removeuser', $usercontext)) {
            $capability  = true;
        }
                
        $out .= $renderer->render_from_template('adminpages_user/passes_table', [
            'cohortpasses' => $this->data,
            'capability' => $capability
        ]);
        $pagdata = $pagination->export_for_template($renderer);
//        $out .= $renderer->render_from_template('theme_franklincovey/pagination', $pagdata);
        return $out;
    }


    // Header
    public function top_banner() {
        // Title
        $banner = \html_writer::start_div('content-header');
        $banner .= \html_writer::tag('h3', get_string( self::$type, 'local_admin'), array('class' => 'title'));
        // Amount
        $banner .= \html_writer::start_div('amount');
        $banner .= \html_writer::span(count($this->data). '<small>Passes</small>');
        
        $banner .= \html_writer::span($this->seatsremaining . '<small>Seats Remaining</small>');
        $banner .= \html_writer::end_div();
        $banner .= \html_writer::end_div();


        return $banner;
    }
    
    public function init() {
        global $DB;
        $cohorts = $DB->get_records('cohort');
        $passvalues = $DB->get_records_sql("select * from {config} where name like 'cohort_%'", null);
        $results = [];
        $cohortids = [];
        if(count($passvalues) > 0) {
            foreach($passvalues as $pass) {
                foreach($cohorts as $cohort) {
                    if($pass->name == 'cohort_start_date_'.$cohort->id ||
                        $pass->name == 'cohort_name_'.$cohort->id ||
                        $pass->name == 'cohort_end_date_'.$cohort->id ||
                        $pass->name == 'cohort_overage_'.$cohort->id ||
                        $pass->name == 'cohort_licenses_'.$cohort->id
                    ) {
                        $cohortids[$cohort->id] = $cohort->id;
                        $field = str_replace('_'.$cohort->id, '', str_replace('cohort_', '', $pass->name));
                        if(!isset($result[$cohort->id][$field])) {
                            $results[$cohort->id][$field] = $pass->value;
                        } else {
                            $results[$cohort->id][$field] = $pass->value;
                        }
                    }
                }
            }
        }
        
        $cohortusers = [];
        if(count($cohortids) > 0) {
            $sql = 'select cm.id as cmid,u.*,cm.cohortid from {cohort_members}  cm join {user} u on u.id =cm.userid where cohortid in ('.implode(',', $cohortids).') ';
        //select e.id, ue.id ueid, ue.status, ue.userid from mdl_enrol e join mdl_user_enrolments ue 
        //on ue.enrolid = e.id  where e.enrol = 'self' and userid=9 and e.customint5=100000001
            $cohortusers = $DB->get_records_sql($sql);
        }

        $cohortpass = [];
        foreach($results as $cohortid => $result) {
            $result['id'] = $cohortid;
            $result['end_date'] = userdate($result['end_date'], '%m/%d/%y');
            $result['users']  = [];
            foreach($cohortusers as $cohortuser) {
                if($cohortid == $cohortuser->cohortid) {
                    $result['users'][] = $cohortuser;
                }
            }
            $result['used'] = count($result['users']);
            $result['available'] = $result['licenses'] - $result['used'];
            $this->seatsremaining += $result['available'];
            $result['isdisabled'] = 'disabled';
            if($result['available'] > 0) {
                $result['isdisabled'] = '';
            }
            $cohortpass[] = $result;
        }
        $this->data = $cohortpass;
    }
}
