<?php

/**
 * @package
 * @subpackage
 * @copyright   2017 Gerry G Hall (gerryghall.co.uk)
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace adminpages_user\local;

/**
 * Team assignment candidates
 */
class candidate_selector extends \user_selector_base {
    protected $teamid;
    protected $context;

    public function __construct($name, $options) {
        $this->teamid = $options['teamid'];
        $this->context = $options['context'];
        parent::__construct($name, $options);
    }


    public function find_users($search) {

        if(!$availableusers = $this->get_users($search)) {
            return [];
        }

        if ($search) {
            $groupname = get_string('currentusersmatching', 'cohort', $search);
        } else {
            $groupname = get_string('currentusers', 'cohort');
        }

        return [$groupname => $availableusers];
    }

    public function get_users ($search) {
        global $DB;
        // By default wherecondition retrieves all users except the deleted, not confirmed and guest.
        list($wherecondition, $params) = $this->search_sql($search, 'u');
        $params['teamid'] = $this->teamid;

        $fields      = 'SELECT ' . $this->required_fields_sql('u');
        $countfields = 'SELECT COUNT(1)';

        $sql = " FROM {user} u
            LEFT JOIN {cohort_members} cm ON (cm.userid = u.id AND cm.cohortid = :teamid)
                WHERE cm.id IS NULL
                AND u.id NOT IN (
                    SELECT userid FROM {cohort_members} WHERE cohortid IN ( SELECT id FROM {cohort} WHERE component = 'team')
                    )
                AND $wherecondition";

        list($sort, $sortparams) = users_order_by_sql('u', $search, $this->accesscontext);
        $order = ' ORDER BY ' . $sort;

        if (!$this->is_validating()) {
            $potentialmemberscount = $DB->count_records_sql($countfields . $sql, $params);
            if ($potentialmemberscount > $this->maxusersperpage) {
                return $this->too_many_results($search, $potentialmemberscount);
            }
        }

        $availableusers = $DB->get_records_sql($fields . $sql . $order, array_merge($params, $sortparams));

        if (empty($availableusers)) {
            return array();
        }
        return $availableusers;
    }

    protected function get_options() {
        $options = parent::get_options();
        $options['teamid'] = $this->teamid;
        $options['file'] = 'local/admin/pages/user/classes/local/candidate_selector.php';
        return $options;
    }
}