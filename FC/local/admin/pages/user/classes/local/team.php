<?php

/**
 * @package     local_admin
 * @subpackage  adminpages_user
 * @copyright   2017 Gerry G Hall (gerryghall.co.uk)
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace adminpages_user\local;

class team implements \renderable, \templatable {

    public $count;
    public $allowed = 0;
    protected $teamid;
    protected $context;
    public $users;
    public $available;
    protected $cohort;

    public function __construct($cohort, $context) {
        global $DB;
        $this->teamid = $cohort->id;
        $this->context = $context;
        $this->cohort = $cohort;
        if ($team = $DB->get_record('team', ['cohortid' => $cohort->id])) {
            $this->allowed = $team->size;
        }
        $this->available = $this->allowed - $this->get_user_count();
    }

    public function __get($name) {

        if (object_property_exists($name, $this->cohort)) {
            return $this->cohort->{$name};
        }

    }

    public function get_user_count() {
        if(!$this->count) {
            global $DB;
            $this->count = $DB->count_records('cohort_members', ['cohortid' => $this->teamid]);
        }
        return $this->count;
    }


    public function export_for_template(\renderer_base $output) {
        $data = new \stdClass();
        $data->count = $this->count;
        $data->available = $this->available;
        $data->allowed = $this->allowed;

        $existinguserselector = new  existing_selector('removeselect', array('teamid'=> $this->teamid, 'accesscontext'=>$this->context));

        foreach ( $existinguserselector->get_users('') as $user ) {
            $data->users[] = new \adminpages_user\user($user);
        }
        return $data;
    }

}