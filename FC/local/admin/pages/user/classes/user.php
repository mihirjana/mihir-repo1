<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   local_admin
 * @copyright 2016 Gerry G Hall
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace adminpages_user;

class user implements \renderable, \templatable {

    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     */
    public $fullname;
    /**
     * @var bool
     */
    public $facilitator;
    /**
     * @var string
     */
    public $pass;
    /**
     * @var string
     */
    public $team;
    /**
     * @var
     */
    public $editurl;

    /*
     * @string
     */
    public $loginasurl;
    /*
     * @var bool
     */
    public $deleted;

    /**
     * @param \stdClass|null $record
     */
    public function __construct(\stdClass $user = null) {


        if (isset($user)) {
            $this->id = $user->id;
            $this->fullname = $user->firstname . ' ' . $user->lastname ;
            $this->facilitator = false;
            $this->pass = 'APP+';
            $this->team = 'Alpha';
            $this->deleted = $user->deleted;
            $this->editurl = new \moodle_url("/user/editadvanced.php" , ['id' => $user->id]);
            $this->loginasurl = new \moodle_url('/course/loginas.php' , ['id' => 1, 'sesskey' => sesskey(), 'user' => $user->id]);
        }
        return $this;
    }



    /**
     * @paramparam \renderer_base $output
     * @return \stdClass
     */
    public function export_for_template(\renderer_base $output) {

        $data = new \stdClass();
        $data->fullname = $this->fullname;
        $data->facilitator = $this->facilitator;
        $data->pass = $this->pass;
        $data->team = $this->team;
        $data->editurl = $this->editurl->out();

        return $data;
    }

}
