<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace adminpages_user;

class users implements \renderable, \templatable {

    public $users;

    static $count;

    public static function get_user_count() {
        if(!self::$count) {
            global $DB;
            self::$count = $DB->count_records('user');
        }
        return self::$count;
    }


    public function __construct(  ) {

    }

    public function load_users($pointer = 0, $perpage = 20) {
        global $DB;
        $users = $DB->get_records("user",[], '','*',$pointer, $perpage);
        $facilitator = $DB->get_field('role', 'id', array('shortname' => 'facilitator'));
        foreach ( $users as $user ) {
            $ruser = new user($user);
            if(user_has_role_assignment($user->id, $facilitator)) {
                $ruser->facilitator = true;
            }

            $this->users[] = $ruser;
        }
        return $this;
    }

    public function  export_for_template(\renderer_base $output) {

        $data = new \stdClass();
        $data->users = $this->users;
        return $data;
    }
}
