<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Core Report class of basic reporting plugin
 * @package    scormreport
 * @subpackage basic
 * @author     Dan Marsden and Ankit Kumar Agarwal
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace adminpages_user;
require_once($CFG->dirroot . '/cohort/lib.php');
defined('MOODLE_INTERNAL') || die();

class teams extends \local_admin\adminpage
{

    static $type = 'user_teams';

    public function __construct(\moodle_page $page)
    {

        parent::__construct($page);
    }

    public function body(\renderer_base $renderer)
    {
        $this->page->requires->js_call_amd('theme_franklincovey/library-switch', 'init');
        $out = '';
        $pagination = new \theme_franklincovey\local\pagination($this->page->url);
        $pagination->set_total(45);
        $pagination->set_rpp(optional_param('rpp', 5, PARAM_INT));
        $pagination->set_current(optional_param('position', 1, PARAM_INT));

        $teams = $this->get_teams(\context_coursecat::instance(5));

        $out .= $renderer->render_from_template('adminpages_user/teams', $teams);

        $this->page->requires->js_call_amd('adminpages_user/add_members','init');

        $d = $this->add_users((object)['id'=>4],\context_coursecat::instance(5));
       // $this->get_all_members();

        //$pagdata = $pagination->export_for_template($renderer);
//        $out .= $renderer->render_from_template('theme_franklincovey/pagination', $pagdata);
        $out .= $renderer->render_from_template(
            'local_admin/modal',
            (object)[
                'id'=> 'members-modal',
                'heading' => 'Add Members',
                'body' => $renderer->render_from_template('adminpages_user/team_form', $d)
            ]
        );
        return $out;
    }

    public function top_banner() {

        // Title
        $banner = \html_writer::start_div('content-header');
        $banner .= \html_writer::tag('h3', get_string(self::$type, 'adminpages_user'), array('class' => 'title'));
        // Amount
        $banner .= \html_writer::start_div('amount');
        $banner .= \html_writer::span(4 . '<small>Teams</small>');
        $banner .= \html_writer::end_div();
        $banner .= \html_writer::end_div();

        // Filter - items per page && search
        $banner .= \html_writer::start_div('table-filter clearfix');
        // Search
        $banner .= \html_writer::start_div('search-field');
        $banner .= \html_writer::tag('input', null, array('class' => 'input-search'));
        $banner .= \html_writer::end_div();
        // Items per page (<select>)
        $icon = \html_writer::tag('i', null, array('class' => 'fa fa-plus-circle'));
        $banner .= \html_writer::link('#', $icon . ' ' . get_string('create_new_team', 'local_admin'),
            array('class' => 'btn btn-leaked'));
        $banner .= \html_writer::end_div();

        return $banner;
    }

    public function get_teams($context, $page = 0, $perpage = 25, $search = '') {

        $teams = [];

        global $DB, $OUTPUT;

        $fields = "SELECT *";
        $countfields = "SELECT COUNT(1)";
        $sql = " FROM {cohort}
             WHERE contextid = :contextid
             AND component = :compt
             ";
        $params = ['contextid' => $context->id, 'compt' => 'team'];
        $order = " ORDER BY name ASC, idnumber ASC";

        if (!empty($search)) {
            list($searchcondition, $searchparams) = cohort_get_search_query($search);
            $sql .= ' AND ' . $searchcondition;
            $params = array_merge($params, $searchparams);
        }

        $totalcohorts = $allcohorts = $DB->count_records('cohort', array('contextid' => $context->id, 'component' => 'team'));
        if (!empty($search)) {
            $totalcohorts = $DB->count_records_sql($countfields . $sql, $params);
        }
        $cohorts = $DB->get_records_sql($fields . $sql . $order, $params, $page*$perpage, $perpage);

        foreach ( $cohorts as $team) {
            $team = new \adminpages_user\local\team($team, $context);
                $teams[] = $team->export_for_template($OUTPUT);
        }

        return array('totalteams' => $totalcohorts, 'teams' => $teams, 'allteams' => $allcohorts);

    }

    public function add_team ($name, $passid, $size=0, $desciption='') {

        $team = new \stdClass();
        $team->name = $name;
        $team->description = $desciption;
        $team->contextid = \context_coursecat::instance(5)->id;
        $team->component = self::$type;
        \cohort_add_cohort($team);
    }

    public function remove_team() {

        //\cohort_delete_cohort()
    }

    protected function add_users($cohort, $context) {
        global $CFG;
        require_once($CFG->dirroot.'/cohort/locallib.php');
        $potentialuserselector = new local\candidate_selector('addselect', array('teamid'=>$cohort->id, 'context'=>$context));

        $existinguserselector = new local\existing_selector('removeselect', array('teamid'=>$cohort->id, 'accesscontext'=>$context));

        $addusers = new \stdClass();
        $addusers->url = $this->page->url;
        $addusers->sesskey = sesskey();
        $addusers->returnurl = $this->page->url;
        $addusers->potentialuserselector = $potentialuserselector->display(true);
        $addusers->existinguserselector = $existinguserselector->display(true);
        return $addusers;

    }


}
