<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Performs user ajax display and filtering.
 * @package     local_admin
 * @copyright   2016 Gerry G Hall (gerryghall.co.uk)
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('AJAX_SCRIPT', true);

require_once(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))) . '/config.php');

$outcome = new stdClass;
$outcome->results = array();
$perpage = optional_param('perpage', 20, PARAM_INT);
$pointer = optional_param('position', 0, PARAM_INT);
//require_sesskey(); // Gotta have the sesskey.
require_login(); // Gotta be logged in (of course).
$PAGE->set_context(context_system::instance());

$courses = array();

echo $OUTPUT->header();

$results = [];
    $users = new \local_admin\local\users();
    $results[] = $users->load_users($pointer, $perpage)->export_for_template($OUTPUT);

$outcome->results = $results;
echo json_encode($outcome);
echo $OUTPUT->footer();
exit;
