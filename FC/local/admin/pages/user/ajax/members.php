<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * ajax file to add and removed member of a team
 * @package     
 * @subpackage  
 * @copyright   2017 Gerry G Hall (gerryghall.co.uk)
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('AJAX_SCRIPT', true);

require_once(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))) . '/config.php');
require_once($CFG->dirroot . '/user/selector/lib.php');
require_once($CFG->dirroot . '/cohort/lib.php');
$outcome = new stdClass;
$outcome->results = array();

//require_sesskey(); // Gotta have the sesskey.
require_login(); // Gotta be logged in (of course).
$teamid = 4; //required_param('teamid',PARAM_INT);
$catid = 5; //required_param('catid',PARAM_INT);
$context = \context_coursecat::instance($catid);
$PAGE->set_context($context);

$existinguserselector = new  adminpages_user\local\existing_selector('removeselect', array('teamid'=> $teamid, 'accesscontext'=>$context));
// Process incoming user assignments to a team
$potentialuserselector = new adminpages_user\local\candidate_selector('addselect', array('teamid'=> $teamid, 'context'=>$context));

$action = optional_param('action','read' ,PARAM_ALPHA);

if ($action == 'add' && confirm_sesskey()) {
    $userstoassign = $potentialuserselector->get_selected_users();
    if (!empty($userstoassign)) {

        foreach ($userstoassign as $adduser) {
            \cohort_add_member($teamid, $adduser->id);
        }

        $potentialuserselector->invalidate_selected_users();
        $existinguserselector->invalidate_selected_users();
    }
}

// Process removing user assignments to the cohort
if ($action == 'remove' && confirm_sesskey()) {
    $userstoremove = $existinguserselector->get_selected_users();
    if (!empty($userstoremove)) {
        foreach ($userstoremove as $removeuser) {
            cohort_remove_member($teamid, $removeuser->id);
        }
        $potentialuserselector->invalidate_selected_users();
        $existinguserselector->invalidate_selected_users();
    }
}


echo $OUTPUT->header();

$results = [];

$results['potentialusers'] = $potentialuserselector->get_users('');
$results['existingusers'] = $existinguserselector->get_users('');

$outcome->results = $results;
echo json_encode($outcome);
echo $OUTPUT->footer();
exit;