<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Performs user ajax display and filtering.
 * @package     local_admin
 * @copyright   2016 Gerry G Hall (gerryghall.co.uk)
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('AJAX_SCRIPT', true);

require_once(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))) . '/config.php');

$outcome = new stdClass;
$outcome->results = array();
$userid = optional_param('id', 0, PARAM_INT);
//require_sesskey(); // Gotta have the sesskey.
require_login(); // Gotta be logged in (of course).
$PAGE->set_context(context_system::instance());

$courses = array();
echo $OUTPUT->header();

$roleid = $DB->get_field('role', 'id', array('shortname' => 'hidelibrary'));

$assignment = $DB->get_record(
    'role_assignments',
    array(
        'contextid' => 1,
        'roleid' => $roleid,
        'userid' => $userid
    )
);

try {
    if($assignment) {
        $DB->delete_records('role_assignments', array('id' => $assignment->id));
        $action = 'on';
    } else {
        $assignment =  new stdClass();
        $assignment->contextid = 1;
        $assignment->roleid = $roleid;
        $assignment->userid = $userid;
        $assignment->timemodified = time();
        $assignment->modifierid = $USER->id;
        $assignment->component = '';
        $assignment->itemid = 0;
        $assignment->sortorder = 0;
        $DB->insert_record('role_assignments', $assignment);
        $action = 'off';
    }
} catch (moodle_exception $e) {
    die(json_encode([
        'status' => false,
        'message' => $e->getMessage()
    ]));
}

echo json_encode([
    'status' => true,
    'action' => $action
]);
exit;
