<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Performs user ajax display and filtering.
 * @package     local_admin
 * @copyright   2016 Gerry G Hall (gerryghall.co.uk)
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('AJAX_SCRIPT', true);

require_once '../../../../../config.php';

$outcome = new stdClass;
$outcome->results = array();
$userid = required_param('userid', PARAM_INT);
$cohortid = required_param('cohortid', PARAM_INT);
//require_sesskey(); // Gotta have the sesskey.
require_login(); // Gotta be logged in (of course).
$PAGE->set_context(context_system::instance());

$courses = array();
echo $OUTPUT->header();

$sql = sprintf(
    "select ue.id, ue.status from {enrol} e 
     join {user_enrolments} ue on ue.enrolid = e.id
     where e.enrol = '%s' and e.customint5 =%d and ue.userid= %d",
    'self',
    $cohortid,
    $userid
);

$userenrol = $DB->get_records_sql($sql);
if($userenrol) {
    $status = 1;
    $action = 'off';
    foreach($userenrol as &$enrol) {
        if($enrol->status) { 
            $status = 0;
            $action = 'on';
        }
        $DB->update_record('user_enrolments', array('id' => $enrol->id, 'status' => $status));
    }
}

echo json_encode(['status' => true, 'action' => $action]);

exit;
