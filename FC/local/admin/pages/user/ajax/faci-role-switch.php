<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Performs user ajax display and filtering.
 * @package     local_admin
 * @copyright   2016 Gerry G Hall (gerryghall.co.uk)
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('AJAX_SCRIPT', true);

require_once '../../../../../config.php';

$userid = required_param('userid', PARAM_INT);
$context = context_system::instance();
//require_sesskey(); // Gotta have the sesskey.
require_login(); // Gotta be logged in (of course).
$PAGE->set_context($context);

$courses = array();
echo $OUTPUT->header();

$action = 'on';
$facilitator = $DB->get_field('role', 'id', array('shortname' => 'facilitator'));
if(user_has_role_assignment($userid, $facilitator)) {
    role_unassign($facilitator, $userid, $context->id);
    $action = 'off';
} else {
    // Assign facilitator role
    role_assign($facilitator, $userid, $context->id);
}


echo json_encode(['status' => true, 'action' => $action]);

exit;
