<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.



define('AJAX_SCRIPT', true);

require_once(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))) . '/config.php');

$cohortid = required_param('cohortid', PARAM_INT);
$searchtext = required_param('searchtext', PARAM_RAW);
//require_sesskey(); // Gotta have the sesskey.
require_login(); // Gotta be logged in of course).
$PAGE->set_context(context_system::instance());

$courses = array();

echo $OUTPUT->header();

$existingusers = $DB->get_records_menu('cohort_members',array('cohortid' => $cohortid),'id', 'id, userid');
if(count($existingusers) > 0) {
    if(!empty($searchtext)) {
        $sql = "select id, firstname, lastname from {user} where id not in (".implode(',', $existingusers).") ".
        " and (firstname like '%$searchtext%' or lastname like '%$searchtext%') ".
        " and (deleted=0 and suspended =0)";
    } else {
        $sql = "select id, firstname, lastname from {user} where id not in (".implode(',', $existingusers).") ".
        " and (deleted=0 and suspended =0)";
    }
} else {
    $sql = "select id, firstname, lastname from {user} ".
    " where (firstname like '%$searchtext%' or lastname like '%$searchtext%') and (deleted=0 and suspended =0)";
}

$users = $DB->get_records_sql($sql);
if(count($users) > 0) {
    echo json_encode([
        'status' => true,
        'users' => $users
    ]);
} else {
    echo json_encode([
        'status' => false,
        'message' => get_string('usernotfound', 'adminpages_user')
    ]);
}
echo $OUTPUT->footer();
exit;
