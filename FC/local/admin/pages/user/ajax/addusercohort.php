<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.



define('AJAX_SCRIPT', true);

require '../../../../../config.php';

$cohortid = required_param('cohortid', PARAM_INT);
$users = required_param_array('users', PARAM_INT);
//require_sesskey(); // Gotta have the sesskey.
require_login(); // Gotta be logged in of course).
$PAGE->set_context(context_system::instance());
$response = ['status' => false];

echo $OUTPUT->header();

$licenses = (int) $DB->get_field(
    'config',
    'value',
    array(
        'name' => 'cohort_calcu_licenses_'.$cohortid
    )
);
$usedlicenses = $DB->count_records('cohort_members', array('cohortid' => $cohortid));
$allowed = $licenses - $usedlicenses;

if(count($users) <= $allowed) {

    $cohortusers = [];
    foreach($users as $user) {
        $insert =  new stdClass();
        $insert->id = null;
        $insert->userid = $user;
        $insert->cohortid = $cohortid;
        $insert->timeadded = time(); 
        $cohortusers[] = $insert;
    }
    
    try {
        $DB->insert_records('cohort_members', $cohortusers);
        $response = [
            'status' => true,
            'message' => get_string('useradded', 'adminpages_user')
        ];
    } catch(dml_write_exception $e) {
        $response = [
            'status' => false,
            'message' => get_string('useralreadyexist', 'adminpages_user')
        ];
    } catch(moodle_exception $e) {
        $response = [
            'status' => false,
            'message' => $e->getMessage()
        ];
    }

} else {
    $response = [
        'status' => false,
        'message' => get_string('maxlicenseslimit', 'adminpages_user')
    ];
}

echo json_encode($response);
exit;
