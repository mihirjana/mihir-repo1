// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

define(['jquery', 'theme_bootstrapbase/bootstrap', 'core/templates'], function($, b, tmp) {

    return {

        init: function() {

            $(document).ready( function() {

                $('#add-members').on('click', function(){
                    $('#members-modal').modal('show');

                });


                $("#assignform").on('click', 'button', function(e){
                    action = e.currentTarget.id;
                    e.preventDefault();

                    var url = M.cfg.wwwroot + '/local/admin/pages/user/ajax/members.php';
                    $('[name=action]').val(action);
                    form = $('#assignform');
                    $.ajax({
                        method: 'POST',
                        url: url,
                        data: form.serialize(),

                        success: function (response, action ) {
                            source = '';

                            $.each( response.results.potentialusers, function( key, value ) {
                                source += "<option value='" + value.id + "'>" + value.firstname + ' ' + value.lastname + "</option>"
                            });
                            ele = $(' #addselect');
                            ele.html(source);

                            source = '';

                            $.each( response.results.existingusers, function( key, value ) {
                                source += "<option value='" + value.id + "'>" + value.firstname + ' ' + value.lastname + "</option>"
                            });
                            ele = $(' #removeselect');
                            ele.html(source);


                        }
                    });

                });

            } );
        }
    };
});

