define(['jquery'], function($) {
    
    function handleresults (response) {
    }
    
    return {
        init: function () {
            $('.faci-role-switch').click(function(){
                var url = M.cfg.wwwroot +'/local/admin/pages/user/ajax/faci-role-switch.php';
                var data = $(this).attr('value');
                
                $.ajax({
                    type: 'post',
                    url: url,
                    data: {'userid': data},
                    success: function(response) {
                        if(response.status) {
                            var tag = $('#faci-role-'+data);
                            if(response.action == 'on') {
                                tag.addClass('on');   
                            } else {
                                tag.removeClass('on');
                            }
                            tag.empty();
                            tag.text(response.action);
                        } else {
                            //error

                        }
                    }
                });
            });
        }    
    };
});
