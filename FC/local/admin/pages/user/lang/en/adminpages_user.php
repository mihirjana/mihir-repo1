<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'admin user' page plugin
 *
 * @package    adminpage
 * @subpackage user
 * @author     Gerry G hall
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['addusers'] = 'Add users';
$string['maxlicenseslimit'] = 'Max licenses reached';
$string['pluginname'] = 'user management';
$string['useradded'] = 'User added successfully';
$string['useralreadyexist'] = 'User is already added';
$string['usernotfound'] = 'User not found';
$string['user_teams'] = 'Teams';
$string['alert'] = 'Alert';
$string['cancel'] = 'Cancel';
$string['confirm'] = 'Confirm';
$string['alertcontent'] = 'You are now  loggedin as another user any activity you perform will be recorded as the user you have loggedin as.';


