<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * 
 * @package     
 * @subpackage  
 * @copyright   2017 Gerry G Hall (gerryghall.co.uk)
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace adminpages_reporting;


class reportblocks implements \renderable, \templatable {

    public $blocks;

    public function __construct( array $tabs ) {

        foreach ($tabs as $tab ) {

            $record = new \stdClass();
            $record->imageurl = '';
            $record->description = '';
            $record->overtext = $tab->title;
            $record->url = $tab->link;
            $this->blocks[] = new reportblock($record);
        }

    }
    public function export_for_template(\renderer_base $output) {
        $data = new \stdClass();
        $data->blocks = $this->blocks;
        return $data;
    }

}