<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Core Report class of basic reporting plugin
 * @package    scormreport
 * @subpackage basic
 * @author     Dan Marsden and Ankit Kumar Agarwal
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace adminpages_scheduling;

defined('MOODLE_INTERNAL') || die();

class active extends \local_admin\adminpage {

    static $type = 'active';

    public function __construct(\moodle_page $page) {
        parent::__construct($page);
    }

    public function top_banner() {

        // Title
        $banner = \html_writer::start_div('content-header');
        $banner .= \html_writer::tag('h3', get_string(self::$type, 'adminpages_scheduling'), array('class' => 'title'));
        // Amount
        $banner .= \html_writer::start_div('amount');
        $banner .= \html_writer::span(3 . '<small>Worksessions</small>');
        $banner .= \html_writer::end_div();
        $banner .= \html_writer::end_div();

        // Filter - items per page && search
        $banner .= \html_writer::start_div('table-filter clearfix');

        // Items per page (<select>)
        $icon = \html_writer::tag('i', null, array('class' => 'fa fa-plus-circle'));
        $banner .= \html_writer::link('#', $icon . ' ' . get_string('create-new', 'adminpages_scheduling'),
            array('class' => 'btn btn-leaked'));
        $banner .= \html_writer::end_div();

        return $banner;
    }

    public function body(\renderer_base $renderer) {
        return $renderer->render_from_template('adminpages_scheduling/active', array());
    }
}
