<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Core Report class of basic reporting plugin
 * @package    scormreport
 * @subpackage basic
 * @author     Dan Marsden and Ankit Kumar Agarwal
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace adminpages_certifications;

defined('MOODLE_INTERNAL') || die();



class certifications extends \local_admin\adminpage {

    static $type = 'certifications';

    public function __construct(\moodle_page $page) {
        parent::__construct($page);
    }

    public function body(\renderer_base $renderer) {
        $data = $this->get_certificate_courses();
        $output = \html_writer::tag('h1', 'Available Programs');
         // Items per page (<select>)
        $output .= \html_writer::start_div('menu-shelf');
        $output .= \html_writer::start_div('order-by');
        $output .= \html_writer::span('SORTY BY:');
        $output .= \html_writer::select(['CERTIFIED',10,20,30], 'qnt_per_page', '30', null, array('class'=>'select-filter', 'placeholder' => 'Search For Users'));
        $output .= \html_writer::end_div();
        $output .= \html_writer::end_div();
        // $output .= '<ul class="shelf-view-card row">';
        // $output .= $renderer->render_from_template('theme_franklincovey/results', $data);
        // $output .= '</ul>';
        $output .= $renderer->render_from_template('adminpages_certifications/certifications_body', $data);
        return $output;
    }

    public function get_certificate_courses() {
        global $DB, $USER;
        $output;

        if( $courses = $DB->get_records('course',['format' => 'certification'])) {
            $results = [];
            foreach ($courses as $course) {
                $results[] = \theme_franklincovey\local\result::from_course($course);
            }
            return new \theme_franklincovey\local\results($results);
        }

        return [];
    }
}