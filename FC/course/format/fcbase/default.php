<?php

return [
    
    'duration' => [
        1 => '30 min or less',
        2 => '30 to 60 min',
        3 => '4 Hours'
    ],
    
    'modality' => [
        1 => 'Live',
        2 => 'Webinar' 
    ],

    'competency' => [
        1 => 'Businesss Acumen',
        2 => 'Communication',
    ],

    'businessneed' => [
        1 => 'Personal Effectiveness',
        2 => 'High trust'
    ],
    
    'jobtobedone' => [
        1 => 'Be more effective',
        2 => 'Manage your time'
    ],

    'curriculum' => [
        1 => '7 habits',
        2 => '5 choices'
    ],

    'productnames' => [
        1 => 'Writing advantage',
        2 => 'Metting advantage'
    ],

    'role' => [
        1 => 'Frontline',
        2 => 'Leaders',
        3 => 'Sales'
    ]
];
