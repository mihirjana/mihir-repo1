<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains main class for the course format impactjourney
 *
 */

defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot. '/course/format/lib.php');

class format_fcbase extends format_base {

    /**
     * Returns true if this course format uses sections
     *
     * @return bool
     */
    public function uses_sections() {
        return true;
    }

    /**
     * The URL to use for the specified course (with section)
     *
     * @param int|stdClass $section Section object from database or just field course_sections.section
     *     if omitted the course view page is returned
     * @param array $options options for view URL. At the moment core uses:
     *     'navigation' (bool) if true and section has no separate page, the function returns null
     *     'sr' (int) used by multipage formats to specify to which section to return
     * @return null|moodle_url
     */
    public function get_view_url($section, $options = array()) {
        global $CFG;
        $course = $this->get_course();
        $url = new moodle_url('/course/view.php', array('id' => $course->id));

        $sr = null;
        if (array_key_exists('sr', $options)) {
            $sr = $options['sr'];
        }
        if (is_object($section)) {
            $sectionno = $section->section;
        } else {
            $sectionno = $section;
        }
        if ($sectionno !== null) {
            if ($sr !== null) {
                if ($sr) {
                    $usercoursedisplay = COURSE_DISPLAY_MULTIPAGE;
                    $sectionno = $sr;
                } else {
                    $usercoursedisplay = COURSE_DISPLAY_SINGLEPAGE;
                }
            } else {
                $usercoursedisplay = $course->coursedisplay;
            }
            if ($sectionno != 0 && $usercoursedisplay == COURSE_DISPLAY_MULTIPAGE) {
                $url->param('section', $sectionno);
            } else {
                if (empty($CFG->linkcoursesections) && !empty($options['navigation'])) {
                    return null;
                }
                $url->set_anchor('section-'.$sectionno);
            }
        }
        return $url;
    }

    /**
     * Returns the information about the ajax support in the given source format
     *
     * The returned object's property (boolean)capable indicates that
     * the course format supports Moodle course ajax features.
     *
     * @return stdClass
     */
    public function supports_ajax() {
        $ajaxsupport = new stdClass();
        $ajaxsupport->capable = true;
        return $ajaxsupport;
    }

    /**
     * Loads all of the course sections into the navigation
     *
     * @param global_navigation $navigation
     * @param navigation_node $node The course node within the navigation
     */
    public function extend_course_navigation($navigation, navigation_node $node) {
        global $PAGE;
        // if section is specified in course/view.php, make sure it is expanded in navigation
        if ($navigation->includesectionnum === false) {
            $selectedsection = optional_param('section', null, PARAM_INT);
            if ($selectedsection !== null && (!defined('AJAX_SCRIPT') || AJAX_SCRIPT == '0') &&
                    $PAGE->url->compare(new moodle_url('/course/view.php'), URL_MATCH_BASE)) {
                $navigation->includesectionnum = $selectedsection;
            }
        }

        // check if there are callbacks to extend course navigation
        parent::extend_course_navigation($navigation, $node);

        // We want to remove the general section if it is empty.
        $modinfo = get_fast_modinfo($this->get_course());
        $sections = $modinfo->get_sections();
        if (!isset($sections[0])) {
            // The general section is empty to find the navigation node for it we need to get its ID.
            $section = $modinfo->get_section_info(0);
            $generalsection = $node->get($section->id, navigation_node::TYPE_SECTION);
            if ($generalsection) {
                // We found the node - now remove it.
                $generalsection->remove();
            }
        }
    }

    /**
     * Custom action after section has been moved in AJAX mode
     *
     * Used in course/rest.php
     *
     * @return array This will be passed in ajax respose
     */
    function ajax_section_move() {
        global $PAGE;
        $titles = array();
        $course = $this->get_course();
        $modinfo = get_fast_modinfo($course);
        $renderer = $this->get_renderer($PAGE);
        if ($renderer && ($sections = $modinfo->get_section_info_all())) {
            
            foreach ($sections as $number => $section) {
                $titles[$number] = $renderer->section_title($section, $course);
            }
        }
        return array('sectiontitles' => $titles, 'action' => 'move');
    }

    /**
     * Returns the list of blocks to be automatically added for the newly created course
     *
     * @return array of default blocks, must contain two keys BLOCK_POS_LEFT and BLOCK_POS_RIGHT
     *     each of values is an array of block names (for left and right side columns)
     */
    public function get_default_blocks() {
        return array(
            BLOCK_POS_LEFT => [],
            BLOCK_POS_RIGHT => []
        );
    }

    /**
     * Definitions of the additional options that this course format uses for course
     *
     * impactjourney format uses the following options:
     * - coursedisplay
     * - numsections
     * - hiddensections
     *
     * @param bool $foreditform
     * @return array of options
     */
    public function course_format_options($foreditform = false) {
        static $courseformatoptions = false;

        if ($courseformatoptions === false) {
            $courseformatoptions = self::get_default_options();
        }
        if ($foreditform && !isset($courseformatoptions['coursedisplay']['label'])) {

            $courseconfig = get_config('moodlecourse');
            $sectionmenu = array();
            for ($i = 0; $i <= $courseconfig->maxsections; $i++) {
                $sectionmenu[$i] = "$i";
            }
            $default = require 'default.php';
            $courseformatoptionsedit = array(
                'numsections' => array(
                    'label' => new lang_string('numberweeks'),
                    'element_type' => 'select',
                    'element_attributes' => array($sectionmenu),
                ),
                'coursedisplay' => array(
                    'label' => new lang_string('coursedisplay'),
                    'element_type' => 'select',
                    'element_attributes' => array(
                        array(
                            COURSE_DISPLAY_SINGLEPAGE => new lang_string('coursedisplay_single'),
                            COURSE_DISPLAY_MULTIPAGE => new lang_string('coursedisplay_multi')
                        )
                    ),
                    'help' => 'coursedisplay',
                    'help_component' => 'moodle',
                ),
                'displayinstructions' => array(
                    'label' => new lang_string('displayinstructions', 'format_fcbase'),
                    'help' => 'displayinstructions',
                    'help_component' => 'format_fcbase',
                    'element_type' => 'select',
                    'element_attributes' => array(
                        array(1 => new lang_string('no'),
                              2 => new lang_string('yes'))
                    )
                )
            );

            $courseformatoptionsedit['duration'] = array(
                'label' => new lang_string('duration', 'format_fcbase'),
                'help' => 'duration',
                'help_component' => 'format_fcbase',
                'element_type' => 'select',
                'element_attributes' => array(
                    $default['duration']
                )
            );

            $courseformatoptionsedit['modality'] = array(
                'label' => new lang_string('modality', 'format_fcbase'),
                'help' => 'modality',
                'help_component' => 'format_fcbase',
                'element_type' => 'select',
                'element_attributes' => array(
                    $default['modality']
                )
            );

            $courseformatoptionsedit['competency'] = array(
                'label' => new lang_string('competency', 'format_fcbase'),
                'help' => 'competency',
                'help_component' => 'format_fcbase',
                'element_type' => 'select',
                'element_attributes' => array(
                    $default['competency']
                )
            );

            $courseformatoptionsedit['businessneed'] = array(
                'label' => new lang_string('businessneed', 'format_fcbase'),
                'help' => 'businessneed',
                'help_component' => 'format_fcbase',
                'element_type' => 'select',
                'element_attributes' => array(
                    $default['businessneed']
                )
            );

            $courseformatoptionsedit['jobtobedone'] = array(
                'label' => new lang_string('jobtobedone', 'format_fcbase'),
                'help' => 'jobtobedone',
                'help_component' => 'format_fcbase',
                'element_type' => 'select',
                'element_attributes' => array(
                    $default['jobtobedone']
                )
            );

            $courseformatoptionsedit['curriculum'] = array(
                'label' => new lang_string('curriculum', 'format_fcbase'),
                'help' => 'curriculum',
                'help_component' => 'format_fcbase',
                'element_type' => 'select',
                'element_attributes' => array(
                    $default['curriculum']
                )
            );

	        $courseformatoptionsedit['duedate'] = array(
                'label' => new lang_string('duedate', 'format_fcbase'),
                'help' => 'duedate',
                'help_component' => 'format_fcbase',
                'element_type' => 'date_selector',
                'element_attributes' => array(
                    array(
                    )
                )
            );

            $courseformatoptionsedit['productnames'] = array(
                'label' => new lang_string('productnames', 'format_fcbase'),
                'help' => 'productnames',
                'help_component' => 'format_fcbase',
                'element_type' => 'select',
                'element_attributes' => array(
                    $default['productnames']
                )
            );

            $courseformatoptionsedit['role'] = array(
                'label' => new lang_string('role', 'format_fcbase'),
                'help' => 'role',
                'help_component' => 'format_fcbase',
                'element_type' => 'select',
                'element_attributes' => array(
                    $default['role']
                )
            );

            $courseformatoptions = array_merge_recursive($courseformatoptions, $courseformatoptionsedit);
        }
        return $courseformatoptions;
    }


    protected static function get_default_options() {
        return array(
            'numsections' => array(
                'default' => 1,
                'type' => PARAM_INT,
            ),
            'coursedisplay' => array(
                'default' => COURSE_DISPLAY_MULTIPAGE,
                'type' => PARAM_INT,
            ),
            'displayinstructions' => array(
                'default' => '',
                'type' => PARAM_INT,
            ),
            'duration' => array(
                'default' => '',
                'type' => PARAM_INT,
            ),
            'modality' => array(
                'default' => '',
                'type' => PARAM_INT,
            ),
            'competency' => array(
                'default' => '',
                'type' => PARAM_INT,
            ),
            'businessneed' => array(
                'default' => '',
                'type' => PARAM_INT,
            ),
            'jobtobedone' => array(
                'default' => '',
                'type' => PARAM_INT,
            ),
            'curriculum' => array(
                'default' => '',
                'type' => PARAM_INT,
            ),
            'productnames' => array(
                'default' => '',
                'type' => PARAM_ALPHA,
            ),
            'role' => array(
                'default' => '',
                'type' => PARAM_INT,
            ),
            'duedate' => array(
                'default' => '',
                'type' => PARAM_INT,
            )

        );
    }

}
