<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Performs user ajax display and filtering.
 * @package     local_admin
 * @copyright   2016 Gerry G Hall (gerryghall.co.uk)
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('AJAX_SCRIPT', true);

require_once '../../../config.php';

$outcome = new stdClass;
$outcome->status = false;
$course = required_param('course', PARAM_INT);
//require_sesskey(); // Gotta have the sesskey.
require_login(); // Gotta be logged in (of course).
$PAGE->set_context(context_system::instance());

try {
    $course = $DB->get_record('course', array('id' => $course), '*', MUST_EXIST);
    $field = 'tnc_agreed_'.$course->id;
    if(!isset($USER->preference[$field])) {
        set_user_preference($field, 1);
        $outcome->status = true;
    }
    $outcome->status = true;
} catch(moodle_exception $e) {
    $outcome->status = false;
    $outcome->error = $e->getMessage();
}
echo json_encode($outcome);
exit;
