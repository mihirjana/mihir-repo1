<?php

function assign_facilitator_role($completed)
{
    global $DB, $CFG;
    $course = get_course($completed->courseid);
    include_once $CFG->dirroot.'/course/format/lib.php';
    $formatoptions = course_get_format($course)->get_format_options();
    if(isset($formatoptions['endorsement']) && !empty($formatoptions['endorsement'])) {
        $assigncourse = $formatoptions['endorsement'];
        $coursecontext = context_course::instance($assigncourse);
        
        $facilitator = $DB->get_field('role', 'id', array('shortname' => 'facilitator'));
        role_assign($facilitator, $completed->relateduserid, $coursecontext->id);
        mtrace('Executing facilitator role for '. $completed->userid);
    }
}
