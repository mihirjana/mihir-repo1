<?php

$observers = array(
    array(
        'eventname' => '\core\event\course_completed',
        'callback' => 'assign_facilitator_role',
        'includefile' => '/course/format/certification/event_handlers.php'
    )
);
