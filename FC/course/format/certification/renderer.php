<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer for outputting the impactjourney course format.
 */


defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot.'/course/format/renderer.php');
require_once($CFG->dirroot . '/course/renderer.php');

class format_certification_renderer extends format_section_renderer_base {

    /**
     * Constructor method, calls the parent constructor
     * @param moodle_page $page
     * @param string      $target one of rendering target constants
     */
    public function __construct(moodle_page $page, $target) {

        parent::__construct($page, $target);

        // Since format_impactjourney_renderer::section_edit_controls() only displays the 'Set current section' control when editing mode is on
        // we need to be sure that the link 'Turn editing mode on' is available for a user who does not have any other managing capability.
        $page->set_other_editing_capability('moodle/course:setcurrentsection');
        //$this->courserenderer = new impactjourney_core_course_renderer($page, $target);
    }

    /**
     * Generate the starting container html for a list of sections
     * @return string HTML to output.
     */
    protected function start_section_list() {

        return html_writer::start_tag('ul', array ('class' => 'topic'));
    }

    /**
     * Generate the closing container html for a list of sections
     * @return string HTML to output.
     */
    protected function end_section_list() {

        return html_writer::end_tag('ul');
    }

    /**
     * Generate the title for this section page
     * @return string the page title
     */
    protected function page_title() {

        return '';
    }

    public function print_single_section_page($course, $sections, $mods, $modnames, $modnamesused, $displaysection) {

        parent::print_single_section_page($course, $sections, $mods, $modnames, $modnamesused, $displaysection);
    }

    public function print_multiple_section_page($course, $sections, $mods, $modnames, $modnamesused) {
        global $CFG, $USER;
        parent::print_multiple_section_page($course, $sections, $mods, $modnames, $modnamesused);

        if(!has_capability('moodle/course:update', context_course::instance($course->id))) { 
            
            if(!isset($USER->preference['tnc_agreed_'.$course->id])) {
                $file = $CFG->dirroot.'/local/admin/pages/standardpages/lib.php';
                if(file_exists($file)) {
                    include_once $file; 
                    
                    $this->page->requires->js_call_amd('format_certification/sitepolicy','init');
                    $page = get_admin_standardpages('tnc');
                    $page->courseid = $course->id;
                    echo parent::render_from_template('format_certification/certification', $page);
                }
            }
        }
    }

}


