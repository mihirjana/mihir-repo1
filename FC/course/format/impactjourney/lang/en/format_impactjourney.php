<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['editsection'] = 'Edit impactjourney';
$string['deletesection'] = 'Delete impactjourney';
$string['sectionname'] = 'impactjourney';
$string['pluginname'] = 'Impact Journey';
$string['section0name'] = 'General';
$string['page-course-view-impactjourney'] = 'Any course main page in impactjourney format';
$string['page-course-view-impactjourney-x'] = 'Any course page in impactjourney format';
$string['hidefromothers'] = 'Hide impactjourney';
$string['showfromothers'] = 'Show impactjourney';
$string['markedthisimpactjourney'] = 'This impactjourney is highlighted as the current one';
$string['markthisimpactjourney'] = 'Highlight this impactjourney as the current one';
$string['impactjourneyoutline'] = 'impactjourney outline';
$string['numbersections'] = 'Number sections';
$string['numbersections_help'] = 'Number sections';
$string['displayinstructions'] = 'Display instructions';
$string['displayinstructions_help'] = 'Display instructions';



//# Elighten FC98 Custom field string
$string['duration'] = 'Duration';
$string['modality'] = 'Modality';
$string['competency'] = 'Competency';
$string['businessneed'] = 'Business need';
$string['jobtobedone'] = 'Job to be done';
$string['curriculum'] = 'Curriculum';
$string['productnames'] = 'Product names';
$string['role'] = 'Role';
$string['duedate'] = 'Due Date';



$string['duration_help'] = 'Duration';
$string['modality_help'] = 'Modality';
$string['competency_help'] = 'Competency';
$string['businessneed_help'] = 'Business need';
$string['jobtobedone_help'] = 'Job to be done';
$string['curriculum_help'] = 'Curriculum';
$string['productnames_help'] = 'Product names';
$string['role_help'] = 'Role';
$string['duedate_help'] = 'Due Date';
$string['feedback'] = 'Feedback';
$string['feedback_help'] = 'Feedback help';
