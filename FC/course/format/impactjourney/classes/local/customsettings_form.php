<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace format_impactjourney\local;
use \moodleform;
use \html_writer;

/**
 * 
 * @package     format_impactjourney
 * @copyright   2016 Gerry G Hall (gerryghall.co.uk)
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class customsettings_form extends moodleform {

    public function definition()
    {
        global $CFG;
        $mform = & $this->_form;
        list($instance) = $this->_customdata;
        $currentimgs = helper::get_format_courseimage($instance->courseid);
        $currentimg = isset($currentimgs[$instance->fileid]) ? $currentimgs[$instance->fileid]['attr']['data-url'] : 'http://placehold.it/250x250';


        $mform->addElement (
            'static',
            'currentimage',
            'Current Image',
            html_writer::img ($currentimg, 'current background',['id' => 'currentimage', 'style' => 'width:250px;height:auto'])
        );


        $values = helper::get_format_courseimage($instance->courseid);
        array_unshift($values, ['name' => get_string('select'), 'value' => 0, 'attr' => []]);
        $bgi = $mform->addElement('select', 'fileid', get_string('backgroundimage', 'format_impactjourney'));

        foreach ($values as $data ) {
            $bgi->addOption($data['name'], $data['value'], $data['attr']);
        }


        $mform->addElement('textarea' , 'caption', get_string('backgroundcaption', 'format_impactjourney' ));

        $mform->addElement('hidden', 'courseid');
        $mform->setType('courseid', PARAM_INT);
        $mform->addElement('hidden', 'cmid');
        $mform->setType('cmid', PARAM_INT);
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        $mform->setDefault('id', 0);

        $this->set_data($instance);

        $this->add_action_buttons();
    }

    public function get_data () {

        $output = parent::get_data ();
        unset($output->submitbutton);
        return $output;

    }
}