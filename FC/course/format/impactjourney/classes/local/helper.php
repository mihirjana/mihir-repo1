<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
namespace format_impactjourney\local;

/**
 * 
 * @package     
 * @subpackage  
 * @copyright   2016 Gerry G Hall (gerryghall.co.uk)
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class helper {

    /**
     * This method
     * is used to populate the options for the custom activity image
     * @return array Of images where the key is the id and the value is the name.
     * @throws \Exception
     * @throws \dml_exception
     */
    static public function get_format_courseimage($courseid){
        $cache = \cache::make('format_impactjourney', 'thumbnails');

        if (!$files = $cache->get($courseid)) {

            $context = \context_course::instance($courseid);
            $fs = get_file_storage();
            $files = array();
            $storedfiles = $fs->get_area_files($context->id, 'format_impactjourney', 'thumbnails',false,'filename',false);
            foreach ($storedfiles as $file) {
                $url = \moodle_url::make_pluginfile_url(
                    $file->get_contextid(),
                    $file->get_component(),
                    $file->get_filearea(),
                    $file->get_itemid(),
                    $file->get_filepath(),
                    $file->get_filename()
                );
                $files[$file->get_id()] = ['name' => $file->get_filename(), 'value' => $file->get_id(), 'attr' => [ 'data-url' => $url->out()]];
            }

            $cache->set($courseid, $files);
        }

        return $files;
    }

    /**
     * The method is responsible for creating/updating course module image mapping records
     * @param $instance
     * @return bool|int
     */
    static public function update_course_module_image ($instance) {
        global $DB;

        if ($DB->record_exists('format_impactjourney_imagemap', ['cmid' => $instance->cmid, 'courseid' => $instance->courseid])) {
            return $DB->update_record('format_impactjourney_imagemap',$instance);
        } else {
            return $DB->insert_record('format_impactjourney_imagemap', $instance);
        }

    }

    static public function get_course_module_metadata($courseid) {
        global $DB;
        $cache = \cache::make('format_impactjourney', 'metadata');
        if (!$data = $cache->get($courseid)) {
            $data =  $DB->get_records ('format_impactjourney_imagemap', ['courseid' => $courseid],'', 'cmid, courseid, fileid, caption');
            $cache->set($courseid, $data);
        }
        return $data;
    }

    /**
     * The method is responsible for creating/updating course module image mapping records
     * @param $instance
     * @return bool|int
     */
    static public function course_module_deleted (\core\event\course_module_deleted $event) {

        global $DB;

        // Purge the caches.
        \cache_helper::invalidate_by_definition('format_impactjourney', 'thumbnails' , $event->courseid);
        \cache_helper::invalidate_by_definition('format_impactjourney', 'metadata' , $event->courseid);

        // Delete all refs for this cm within the format database table

        $DB->delete_records('format_impactjourney_imagemap', ['cmid' => $event->objectid]);


    }

    static public function course_deleted(\core\event\course_deleted $event) {
        global $DB;

        // Purge the caches.
        \cache_helper::invalidate_by_definition('format_impactjourney', 'thumbnails' , $event->courseid);
        \cache_helper::invalidate_by_definition('format_impactjourney', 'metadata' , $event->courseid);

        // Delete all save images for this format / course.
        $context = \context_course::instance($event->courseid);
        $fs = get_file_storage();
        $storedfiles = $fs->get_area_files($context->id, 'format_impactjourney', 'thumbnails',false,'filename',false);
        foreach ($storedfiles as $file) {
            $file->delete();
        }

        // Delete all refs for this course within the format database table

        $DB->delete_records('format_impactjourney_imagemap', ['courseid' => $event->courseid]);

    }

    static public function course_updated(\core\event\course_updated $event) {
        \cache_helper::invalidate_by_definition('format_impactjourney', 'thumbnails' , $event->courseid);
    }

}