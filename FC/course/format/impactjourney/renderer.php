<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer for outputting the impactjourney course format.
 */


defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot.'/course/format/renderer.php');
require_once($CFG->dirroot . '/course/renderer.php');

class format_impactjourney_renderer extends format_section_renderer_base {

    /**
     * Constructor method, calls the parent constructor
     *
     * @param moodle_page $page
     * @param string $target one of rendering target constants
     */
    public function __construct(moodle_page $page, $target) {
        parent::__construct($page, $target);

        // Since format_impactjourney_renderer::section_edit_controls() only displays the 'Set current section' control when editing mode is on
        // we need to be sure that the link 'Turn editing mode on' is available for a user who does not have any other managing capability.
        $page->set_other_editing_capability('moodle/course:setcurrentsection');
        //$this->courserenderer = new impactjourney_core_course_renderer($page, $target);
    }

    /**
     * Generate the starting container html for a list of sections
     * @return string HTML to output.
     */
    protected function start_section_list() {
        return html_writer::start_tag('ul', array('class' => 'impactjourney'));
    }

    /**
     * Generate the closing container html for a list of sections
     * @return string HTML to output.
     */
    protected function end_section_list() {
        return html_writer::end_tag('ul');
    }

    /**
     * Generate the title for this section page
     * @return string the page title
     */
    protected function page_title() {
        return get_string('impactjourneyoutline', 'format_impactjourney');
    }
    public function print_single_section_page($course, $sections, $mods, $modnames, $modnamesused, $displaysection) {
        global $PAGE;


        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();

        // Can we view the section in question?
        if (!($sectioninfo = $modinfo->get_section_info($displaysection))) {
            // This section doesn't exist
            print_error('unknowncoursesection', 'error', null, $course->fullname);
            return;
        }

        if (!$sectioninfo->uservisible) {
            if (!$course->hiddensections) {
                echo $this->start_section_list();
                echo $this->section_hidden($displaysection, $course->id);
                echo $this->end_section_list();
            }
            // Can't view this section.
            return;
        }


        // Copy activity clipboard..
        echo $this->course_activity_clipboard($course, $displaysection);
        $thissection = $modinfo->get_section_info(0);
        if ($thissection->summary or ! empty($modinfo->sections[0]) or $PAGE->user_is_editing()) {
            echo $this->start_section_list();
            echo $this->section_header($thissection, $course, true, $displaysection);
            echo $this->courserenderer->course_section_cm_list($course, $thissection, $displaysection);
            echo $this->courserenderer->course_section_add_cm_control($course, 0, $displaysection);
            echo $this->section_footer();
            echo $this->end_section_list();
        }

        // Start single-section div
        echo html_writer::start_tag('div', array('class' => 'single-section'));

        // Now the list of sections..
        echo $this->start_section_list();
        echo html_writer::start_div('span6', array('style' => 'background-color:#53893E;color:white;padding:20px'));
        echo html_writer::tag('h2', $course->fullname);
        echo html_writer::empty_tag('hr', array('class' => 'divider'));
        echo html_writer::tag('p', $course->summary);
        echo html_writer::empty_tag('br');
        echo html_writer::empty_tag('br');
        echo html_writer::alist(array(' My Projects', 'Help Requests', ' Project Evaluations', ' Community Questions'), array('class' => 'list-group'));
        echo html_writer::end_div();

        echo html_writer::start_div('span6');
        // The requested section page.
        $thissection = $modinfo->get_section_info($displaysection);

        // Title with section navigation links.
        $sectionnavlinks = $this->get_nav_links($course, $modinfo->get_section_info_all(), $displaysection);
        $sectiontitle = '';
        $sectiontitle .= html_writer::start_tag('div', array('class' => 'section-navigation navigationtitle'));
        $sectiontitle .= html_writer::tag('span', $sectionnavlinks['previous'], array('class' => 'mdl-left'));
        $sectiontitle .= html_writer::tag('span', $sectionnavlinks['next'], array('class' => 'mdl-right'));
        // Title attributes
        $classes = 'sectionname';
        if (!$thissection->visible) {
            $classes .= ' dimmed_text';
        }
        $sectiontitle .= $this->output->heading(get_section_name($course, $displaysection), 3, $classes);

        $sectiontitle .= html_writer::end_tag('div');
        echo $sectiontitle;
        echo $this->start_section_list();
        echo $this->section_header($thissection, $course, true, $displaysection);
        $completioninfo = new completion_info($course);
        echo $completioninfo->display_help_icon();

        echo $this->courserenderer->course_section_cm_list($course, $thissection, $displaysection);
        echo $this->courserenderer->course_section_add_cm_control($course, $displaysection, $displaysection);
        echo $this->section_footer();
        echo $this->end_section_list();

        // Display section bottom navigation.
        $sectionbottomnav = '';
        $sectionbottomnav .= html_writer::start_tag('div', array('class' => 'section-navigation mdl-bottom'));
        $sectionbottomnav .= html_writer::tag('span', $sectionnavlinks['previous'], array('class' => 'mdl-left'));
        $sectionbottomnav .= html_writer::tag('span', $sectionnavlinks['next'], array('class' => 'mdl-right'));
        $sectionbottomnav .= html_writer::tag('div', $this->section_nav_selection($course, $sections, $displaysection), array('class' => 'mdl-align'));
        $sectionbottomnav .= html_writer::end_tag('div');
        echo $sectionbottomnav;

        // Close single-section div.
        echo html_writer::end_tag('div');
    }

    public function print_multiple_section_page($course, $sections, $mods, $modnames, $modnamesused) {
        global $PAGE, $DB, $CFG, $USER;


        if($this->is_course_complete($course)) {
            $formatoption = course_get_format($course)->get_course();
            if(isset($formatoption->feedback)) {
                redirect(new moodle_url(
                    $CFG->wwwroot.'/mod/feedback/view.php',
                    array(
                        'id' => $formatoption->feedback
                    )
                ));
                return;
            }
        }
        $attachcourses = $DB->get_records_menu('course_completion_criteria', [
            'course' => $course->id,
            'criteriatype' => COMPLETION_CRITERIA_TYPE_COURSE
        ], 'id asc','id, courseinstance');

         // Check if any course attached for this course
        if(count($attachcourses) == 0) {
            parent::print_multiple_section_page($course, $sections, $mods, $modnames, $modnamesused);
            return;
        }

        $sql = sprintf(
            'select course from {course_completions} where course in (%s) and (userid = %d and timecompleted is not null)',
            implode(',', $attachcourses),
            $USER->id
        );
        $completedcourses = $DB->get_records_sql($sql);
        list($usql, $params) = $DB->get_in_or_equal($attachcourses);
        $formatoptions =  $DB->get_records_select('course_format_options', "courseid  {$usql} ", $params);
        $options = [];
        foreach($formatoptions as $option) {
            if(!isset($options[$option->courseid])) {
                $options[$option->courseid][$option->name] = $option->value;
            } else {
                $options[$option->courseid][$option->name] = $option->value;
            }
        }
        //print_object($options);
        foreach ($attachcourses as $courseid) {
            
            $course = get_course($courseid);
            $modinfo = get_fast_modinfo($course);
            $course = course_get_format($course)->get_course();
            $completion = $this->course_completion($course);
            echo html_writer::start_div('row impactjourney');
            echo html_writer::start_div('col-md-12');
            echo html_writer::start_div('panel panel-default panel-horizontal');
            echo html_writer::start_div('panel-heading');
            if(isset($completedcourses[$courseid])) {
                echo html_writer::tag('h2', html_writer::empty_tag('input', [
                    'type' => 'checkbox', 'checked' => 'checked'
                ]) . $course->fullname, ['class' => 'panel-title']);
            } else {
                echo html_writer::tag('h2', html_writer::empty_tag('input', [
                    'type' => 'checkbox'
                ]) . $course->fullname, ['class' => 'panel-title']);
            }
            if(!is_null($completion['completeddate'])) { 
                echo html_writer::tag('p', $completion['status']. ' '. userdate($completion['completeddate'], '%m/%d/%y'));
            } else {
                echo html_writer::tag('p', $completion['status']);
            }
            //echo html_writer::tag('p', $completion['complete'].' / '.$completion['totalactivity']);

            echo html_writer::end_div();
            echo html_writer::start_div('panel-body');
            $context = context_course::instance($course->id);
            // Title with completion help icon.
            $completioninfo = new completion_info($course);
            echo $completioninfo->display_help_icon();
            echo $this->output->heading($this->page_title(), 2, 'accesshide');

            // Copy activity clipboard..
            echo $this->course_activity_clipboard($course, 0);

            // Now the list of sections..
            echo $this->start_section_list();

            foreach ($modinfo->get_section_info_all() as $section => $thissection) {
                if ($section == 0) {
                    // 0-section is displayed a little different then the others
                    if ($thissection->summary or !empty($modinfo->sections[0]) or $PAGE->user_is_editing()) {
                        echo $this->section_header($thissection, $course, false, 0);
                        echo $this->courserenderer->course_section_cm_list($course, $thissection, 0);
                        echo $this->courserenderer->course_section_add_cm_control($course, 0, 0);
                        echo $this->section_footer();
                    }
                    continue;
                }
                if ($section > $course->numsections) {
                    // activities inside this section are 'orphaned', this section will be printed as 'stealth' below
                    continue;
                }
                // Show the section if the user is permitted to access it, OR if it's not available
                // but there is some available info text which explains the reason & should display.
                $showsection = $thissection->uservisible ||
                        ($thissection->visible && !$thissection->available &&
                        !empty($thissection->availableinfo));
                if (!$showsection) {
                    // If the hiddensections option is set to 'show hidden sections in collapsed
                    // form', then display the hidden section message - UNLESS the section is
                    // hidden by the availability system, which is set to hide the reason.
                    if (!$course->hiddensections && $thissection->available) {
                        echo $this->section_hidden($section, $course->id);
                    }

                    continue;
                }

                if (!$PAGE->user_is_editing() && $course->coursedisplay == COURSE_DISPLAY_MULTIPAGE) {
                    // Display section summary only.
                    echo $this->section_summary($thissection, $course, null);
                } else {
                    echo $this->section_header($thissection, $course, false, 0);
                    if ($thissection->uservisible) {
                        echo $this->courserenderer->course_section_cm_list($course, $thissection, 0);
                        echo $this->courserenderer->course_section_add_cm_control($course, $section, 0);
                    }
                    echo $this->section_footer();
                }
            }

            if ($PAGE->user_is_editing() and has_capability('moodle/course:update', $context)) {
                // Print stealth sections if present.
                foreach ($modinfo->get_section_info_all() as $section => $thissection) {
                    if ($section <= $course->numsections or empty($modinfo->sections[$section])) {
                        // this is not stealth section or it is empty
                        continue;
                    }
                    echo $this->stealth_section_header($section);
                    echo $this->courserenderer->course_section_cm_list($course, $thissection, 0);
                    echo $this->stealth_section_footer();
                }

                echo $this->end_section_list();

                echo html_writer::start_tag('div', array('id' => 'changenumsections', 'class' => 'mdl-right'));

                // Increase number of sections.
                $straddsection = get_string('increasesections', 'moodle');
                $url = new moodle_url('/course/changenumsections.php',
                    array('courseid' => $course->id,
                          'increase' => true,
                          'sesskey' => sesskey()));
                $icon = $this->output->pix_icon('t/switch_plus', $straddsection);
                echo html_writer::link($url, $icon.get_accesshide($straddsection), array('class' => 'increase-sections'));

                if ($course->numsections > 0) {
                    // Reduce number of sections sections.
                    $strremovesection = get_string('reducesections', 'moodle');
                    $url = new moodle_url('/course/changenumsections.php',
                        array('courseid' => $course->id,
                              'increase' => false,
                              'sesskey' => sesskey()));
                    $icon = $this->output->pix_icon('t/switch_minus', $strremovesection);
                    echo html_writer::link($url, $icon.get_accesshide($strremovesection), array('class' => 'reduce-sections'));
                }

                echo html_writer::end_tag('div');
            } else {
                echo $this->end_section_list();
            }
            echo  html_writer::end_div('div');
            echo  html_writer::end_div('div');
            echo  html_writer::end_div('div');
            echo  html_writer::end_div('div');
        }
    }


    public function is_course_complete($course) {
        global $USER;
        $info = new completion_info($course);
        return $info->is_course_complete($USER->id);
    }

    public function course_completion($course) {
        global $USER, $USER;
        
        $info = new completion_info($course);
        // Load criteria to display.
        $completions = $info->get_completions($USER->id);
        // Generate markup for criteria statuses.
        $data = [];

        // For aggregating activity completion.
        $activities = array();
        $activities_complete = 0;

        // For aggregating course prerequisites.
        $prerequisites = array();
        $prerequisites_complete = 0;

        // Flag to set if current completion data is inconsistent with what is stored in the database.
        $pending_update = false;
        // Loop through course criteria.
        foreach ($completions as $completion) {
            
            $criteria = $completion->get_criteria();
            $complete = $completion->is_complete();
            if (!$pending_update && $criteria->is_pending($completion)) {
                $pending_update = true;
            }
            
            // Activities are a special case, so cache them and leave them till last.
            if ($criteria->criteriatype == COMPLETION_CRITERIA_TYPE_ACTIVITY) {
                $activities[$criteria->moduleinstance] = $complete;
                if ($complete) {
                    $activities_complete++;
                }
                continue;
            }
            
            // Prerequisites are also a special case, so cache them and leave them till last.
            if ($criteria->criteriatype == COMPLETION_CRITERIA_TYPE_COURSE) {
                $prerequisites[$criteria->courseinstance] = $complete;
                if ($complete) {
                    $prerequisites_complete++;
                }
                
                continue;
            }
        }
        
        // Is course complete?
        $coursecomplete = $info->is_course_complete($USER->id);
        
        // Load course completion.
        $params = array(
            'userid' => $USER->id,
            'course' => $course->id
        );
        
        $ccompletion = new completion_completion($params);
        // Has this user completed any criteria?
        $criteriacomplete = $info->count_course_user_data($USER->id);
         
        if ($pending_update) {
            $content = get_string('pending', 'completion');
        } else if ($coursecomplete) {
            $content = get_string('complete');
        } else if (!$criteriacomplete && !$ccompletion->timestarted) {
            $content = get_string('notyetstarted', 'completion');
        } else {
            $content = get_string('inprogress', 'completion');
        }

        return [
            'status' => $content,
            'totalactivity' => count($activities),
            'complete' => $activities_complete,
            'completeddate' => $ccompletion->timecompleted
        ];
    }
}
