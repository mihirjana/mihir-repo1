// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Creates A SVG donut user course completion
 *
 * @module     format_impactjourney/donut
 * @package    format_impactjourney
 * @copyright  2016 Gerry G Hall
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
define(['jquery'], function($) {

    var donut = function (percentage, size, backgroundColour, foregroundColour, fontColour, centerColour) {

        var svgns = "http://www.w3.org/2000/svg";
        var chart = document.createElementNS(svgns, "svg:svg");
        chart.setAttribute("width", size);
        chart.setAttribute("height", size);
        chart.setAttribute("viewBox", "0 0 " + size + " " + size);

        // Background circle.
        var back = document.createElementNS(svgns, "circle");
        back.setAttributeNS(null, "cx", size / 2);
        back.setAttributeNS(null, "cy", size / 2);
        back.setAttributeNS(null, "r", size / 2);
        back.setAttributeNS(null, "fill", backgroundColour);
        chart.appendChild(back);

        // Primary.
        var path = document.createElementNS(svgns, "path");
        var unit = (Math.PI * 2) / 100;
        var startangle = 0;
        var endangle = percentage * unit - 0.001;
        var x1 = (size / 2) + (size / 2) * Math.sin(startangle);
        var y1 = (size / 2) - (size / 2) * Math.cos(startangle);
        var x2 = (size / 2) + (size / 2) * Math.sin(endangle);
        var y2 = (size / 2) - (size / 2) * Math.cos(endangle);
        var big = 0;
        if (endangle - startangle > Math.PI) {
            big = 1;
        }
        var d = "M " + (size / 2) + "," + (size / 2) +
                " L " + x1 + "," + y1 +
                " A " + (size / 2) + "," + (size / 2) +
                " 0 " + big + " 1 " + x2 + "," + y2 +
                " Z";
        path.setAttribute("d", d); // Set this path
        path.setAttribute("fill", foregroundColour);

        chart.appendChild(path); // Add wedge to chart
        // Foreground.
        var front = document.createElementNS(svgns, "circle");
        front.setAttributeNS(null, "cx", (size / 2));
        front.setAttributeNS(null, "cy", (size / 2));
        front.setAttributeNS(null, "r", (size * 0.35));
        front.setAttributeNS(null, "fill", centerColour);
        chart.appendChild(front);
        var percent = document.createElementNS(svgns, "text");
        var t = document.createTextNode(percentage.toString() + "%");
        var fontsize = 14;
        percent.setAttributeNS(null, "fill", fontColour);
        percent.setAttributeNS(null, "alignment-baseline", "central");
        percent.setAttributeNS(null, "text-anchor", "middle");
        percent.setAttributeNS(null, "font-size", fontsize.toString() + "px");
        percent.appendChild(t);
        percent.setAttributeNS(null, "x", (size / 2));
        percent.setAttributeNS(null, "y", (size / 2)+6);
        chart.appendChild(percent);
        return chart;
    };

    return {

        init: function() {

            $(document).ready( function() {

                $(".completion-donut").each(function(){
                    $(this).append(donut(
                        $(this).data('percentage') || 0,
                        $(this).data('size') || 50,
                        $(this).data('backgroundcolour') || '#ccc',
                        $(this).data('foregroundcolour') || '#fff',
                        $(this).data('fontcolour') || '#fff',
                        $(this).data('centercolour') || '#000'
                    ));
                });


            } );
        }
    };
});

