<?php

// This file is part of Moodle - http://moodle.org/
// //
// // Moodle is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// //
// // Moodle is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //
// // You should have received a copy of the GNU General Public License
// // along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Recommended assets
 *
 * Class implmentation
 *
 * @package     block
 * @subpackage  recommendedassets
 * @author      Shambhu Kumar
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */



class block_recommendedassets extends block_base
{

    /**
     * Init the block
     */
    function init()
    {
        $this->title = get_string('pluginname', 'block_recommendedassets');
    }

    /**
     * Applicable formats
     * @return array
     */
    function applicable_formats()
    {
        return array('all' => true);
    }

    function instance_allow_multiple()
    {
        return false;
    }

    /**
     * Return the content
     * @global stdClass $CFG
     * @return string
     */
    function get_content()
    {
        global $CFG, $OUTPUT;

        require_once($CFG->dirroot . '/course/lib.php');
        $course = $this->page->course;

        if ($this->content !== NULL)
        {
            return $this->content;
        }

        $this->content = new stdClass;

        $this->content->text = html_writer::tag('p',get_string('linkdesc', 'block_recommendedassets'),
            array('class' => 'text-center')
        );
        $this->content->text .= $OUTPUT->action_link(
            $CFG->wwwroot.'/blocks/recommendedassets/results.php',
            get_string('button', 'block_recommendedassets'),
            null,
            array(
                'class' => 'btn btn-primary'
            )
        );

        return $this->content;
    }

    /**
     * The block should only be dockable when the title of the block is not empty
     * and when parent allows docking.
     *
     * @return bool
     */
    public function instance_can_be_docked()
    {
        return (!empty($this->title) && parent::instance_can_be_docked());
    }

}
