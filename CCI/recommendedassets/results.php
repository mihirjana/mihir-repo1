<?php

// This file is part of Moodle - http://moodle.org/
// //
// // Moodle is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// //
// // Moodle is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //
// // You should have received a copy of the GNU General Public License
// // along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Recommended assets
 *
 * Tag search results
 *
 * @package     block
 * @subpackage  recommendedassets
 * @author      Shambhu Kumar
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require('../../config.php');
require($CFG->dirroot.'/user/profile/lib.php');

require_login(0, false);
$systemcontext = context_system::instance();
$PAGE->set_context($systemcontext);
$PAGE->set_pagelayout('incourse');
$PAGE->set_url('/blocks/recommendedassets/results.php');
$PAGE->set_title(get_string('pluginname', 'block_recommendedassets'));
$PAGE->set_heading(get_string('pluginname', 'block_recommendedassets'));
echo $OUTPUT->header();
profile_load_custom_fields($USER);
if(count($USER->profile) > 0) {
    if(isset($USER->profile['IntheCourts']))
        unset($USER->profile['IntheCourts']);
    $fields = "'" . implode("','", $USER->profile) . "'";
    $fields = strtolower($fields);
    $sql = "SELECT ti.id,ti.itemid,t.id as tagid,t.name, t.rawname from {tag} t ".
        "join {tag_instance} ti on ti.tagid = t.id ".
        "where ti.itemtype='course_modules' AND userid=?  AND t.name in (".$fields.") order by t.id";
    $tags = $DB->get_records_sql($sql, array($USER->id));
    if(count($tags) > 0) {
        $moduleids = [];
        array_map(function($tag) use (&$moduleids) {
            $moduleids[] = $tag->itemid;
        }, $tags);
        $modules = $DB->get_records_menu('modules');
        list($in, $params) = $DB->get_in_or_equal($moduleids);
        $cms = $DB->get_recordset_select('course_modules', "id $in", $params, 'id, module, instance');
        $results = [];
        $modulemap = [];
        foreach($cms as $cm) {
            $results[$cm->id] = $DB->get_field($modules[$cm->module], 'name', array('id' => $cm->instance));
            $modulemap[$cm->id] = $modules[$cm->module];
        }
        $table = new html_table();
        $table->head = (array) get_strings(['title', 'type'], 'block_recommendedassets');
        $uniquetag = 0;
        foreach($tags as $tag) {
            if($uniquetag ==0 || $uniquetag != $tag->tagid) {
                $row = new html_table_row();
                $field = array_search($tag->rawname, $USER->profile);
                $cell = new html_table_cell(html_writer::tag('strong', $field.' [ '.$tag->rawname.' ]'));
                $cell->colspan = 2;
                $row->cells[] = $cell;
                $table->data[] = $row;
                $uniquetag = $tag->tagid;
            }
            $table->data[] = array(
                html_writer::link(
                    new moodle_url(
                        $CFG->wwwroot.'/mod/'.$modulemap[$tag->itemid].'/view.php',
                        array(
                            'id' => $tag->itemid
                        )
                    ),
                    $results[$tag->itemid]
                ),
                ucfirst($modulemap[$tag->itemid])
            );
        }
        echo html_writer::table($table);
    } else {
        echo html_writer::div(
            get_string('noresults', 'block_recommendedassets'),
            'alert alert-warning'
        );
    }
} else {
    echo html_writer::div(
        get_string('noresults', 'block_recommendedassets'),
        'alert alert-warning'
    );
}
echo $OUTPUT->footer();
