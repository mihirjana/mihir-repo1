<?php
/**
 * *************************************************************************
 * *                       HPCL - Competency Search                          **
 * *************************************************************************
 * @package     block                                                   **
 * @subpackage  HPCL competencysearch                                        **
 * @name        HPCL Competency Search                                       **
 * @copyright   Dhruv Infoline Pvt Ltd                                  **
 * @author      Arjun Singh (arjunsingh@elearn10.com)                   **
 * @license     http://lmsofindia.com                                   **
 * *************************************************************************
 * ************************************************************************ */





defined('MOODLE_INTERNAL') || die();

$plugin->release    = '2.0.3 (Build: 2014040200)';
$plugin->version    = 2016112210;
$plugin->requires   = 2015102300; // Moodle 3.0 and above.
$plugin->component = 'block_hpcl_competencysearch';  
$plugin->maturity   = MATURITY_STABLE;
//$plugin->cron = 0;
