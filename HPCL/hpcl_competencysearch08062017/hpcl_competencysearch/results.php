<?php
/**
 * *************************************************************************
 * *                       HPCL - Competency Search                          **
 * *************************************************************************
 * @package     block                                                   **
 * @subpackage  cci competencysearch                                        **
 * @name        CCI Global Search                                       **
 * @copyright   Dhruv Infoline Pvt Ltd                                  **
 * @author      Arjun Singh (arjunsingh@elearn10.com)                   **
 * @license     http://lmsofindia.com                                   **
 * *************************************************************************
 * ************************************************************************ */
require('../../config.php');
require('lib.php');
$q = optional_param('q', array(), PARAM_TEXT);
$sbu = optional_param('sbu', array(), PARAM_TEXT);
$role = optional_param('role', array(), PARAM_TEXT);
$practicelevel = optional_param('practicelevel', array(), PARAM_TEXT);
$competency = optional_param('competency', array(), PARAM_TEXT);

require_login();
global $CFG, $PAGE, $OUTPUT, $DB, $USER;
$systemcontext = context_system::instance();
$PAGE->set_context($systemcontext);
$PAGE->set_pagelayout('standard');
$PAGE->set_url($CFG->wwwroot . '/blocks/hpcl_competencysearch/results.php');
$pagetitle = $PAGE->set_title(get_string('pagetitle', 'block_hpcl_competencysearch'));
$PAGE->set_heading(get_string('pagetitle', 'block_hpcl_competencysearch'));
$PAGE->requires->css('/blocks/hpcl_competencysearch/styles.css');

$searchlink = html_writer::link(new moodle_url('/blocks/hpcl_competencysearch/results.php'),get_string('competency_search','block_hpcl_competencysearch'));
$node = $PAGE->navbar->add($searchlink);
if($q == NULL){
    $q = '0';
    $node->add(get_string('newsearch', 'block_hpcl_competencysearch'));
}else{
    $node->add(get_string('searchresult', 'block_hpcl_competencysearch'));
} 
echo $OUTPUT->header();

$othertag_search = block_hpcl_competencysearch_othertags($q,$sbu,$role,$practicelevel,$competency);
echo "<div class=\"clearfix\">";
echo "<div class=\"info\">";
if($q != NULL){
    $content = '';
    $cresult = get_string('course_results', 'block_hpcl_competencysearch');
    $cr = get_string('cr','block_hpcl_competencysearch');
    $courses = $DB->get_records('course');
    $categoroies = $DB->get_records('course_categories');
    unset($courses[1]);
    $content .= "<ul class='block_hpcl_competencysearch_result_list'>";
    $count = 0;
    $printed = false;
    $flag = 0;
    $course_tag_result =  array();
    foreach ($courses as $courseid => $course) {
        $course_tag_result = block_hpcl_competencysearch_course_tags($course->id, $othertag_search);
        if($course_tag_result != ''){
            $content .= $course_tag_result;
            $count = count($course_tag_result);
            $count = $count + 1;
        }
    }
    $content .= "</ul>";
    if ($count == 0) {
        $q = ' ';
        echo "<h4 class='search_title_noresult'>" .$cresult."</h4>";
        echo '<i>' . get_string('no_result', 'block_hpcl_competencysearch') . '</i>';
    }else{
        echo"<h4 class='search_title_result'>" .$cresult."</h4>";
        echo $content;
    }
    echo "<hr/>";
    echo $course_searcharea = block_hpcl_competencysearch_searcharea(1);
}else{
    echo $course_searcharea = block_hpcl_competencysearch_searcharea(0);
}
echo "</div>";
echo "</div>";
echo $OUTPUT->footer();
