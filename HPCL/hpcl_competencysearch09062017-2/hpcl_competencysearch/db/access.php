<?php
/**
 * HPCL Competency Search Block Caps.
 *
 * @package    block_hpcl_competencysearch
 * @copyright  Arjun Singh <arjunsingh@elearn10.com>
 * @license    http://lmsofindia.com 2017 or later
 */

defined('MOODLE_INTERNAL') || die();

$capabilities = array(

    'block/hpcl_competencysearch:myaddinstance' => array(
        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
            'user' => CAP_ALLOW
        ),

        'clonepermissionsfrom' => 'moodle/my:manageblocks'
    ),

    'block/hpcl_competencysearch:addinstance' => array(
        'riskbitmask' => RISK_SPAM | RISK_XSS,
        'captype' => 'write',
        'contextlevel' => CONTEXT_BLOCK,
        'archetypes' => array(
            'editingteacher' => CAP_ALLOW,
            'manager' => CAP_ALLOW
        ),

        'clonepermissionsfrom' => 'moodle/site:manageblocks'
    ),
);
