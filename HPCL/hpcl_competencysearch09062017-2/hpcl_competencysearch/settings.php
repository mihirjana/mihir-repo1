<?php

/**
/**
 * *************************************************************************
 * *                       HPCL - Competency Search Block Settings                         **
 * *************************************************************************
 * @package     block                                                   **
 * @subpackage  HPCL competencysearch                                        **
 * @name        HPCL competency Search                                       **
 * @copyright   Dhruv Infoline Pvt Ltd                                  **
 * @author      Arjun Singh (arjunsingh@elearn10.com)                   **
 * @license     http://lmsofindia.com                                   **
 * *************************************************************************
 * ************************************************************************ */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    $settings->add(new admin_setting_configtext('block_hpcl_competencysearch/sbulist', new lang_string('sbulist', 'block_hpcl_competencysearch'),
        new lang_string('sbulisthint', 'block_hpcl_competencysearch'), null, PARAM_RAW));
    $settings->add(new admin_setting_configcheckbox('block_hpcl_competencysearch/sbusearch', new lang_string('sbusearch', 'block_hpcl_competencysearch'),null, 0, 1));

    $settings->add(new admin_setting_configtext('block_hpcl_competencysearch/rolelist', new lang_string('rolelist', 'block_hpcl_competencysearch'),
        new lang_string('rolelisthint', 'block_hpcl_competencysearch'), null, PARAM_RAW));
    $settings->add(new admin_setting_configcheckbox('block_hpcl_competencysearch/rolesearch', new lang_string('rolesearch', 'block_hpcl_competencysearch'),null, 0, 1));

    $settings->add(new admin_setting_configtext('block_hpcl_competencysearch/practicelevel', new lang_string('practicelevel', 'block_hpcl_competencysearch'),
        new lang_string('practicelevelhint', 'block_hpcl_competencysearch'), null, PARAM_RAW));
    $settings->add(new admin_setting_configcheckbox('block_hpcl_competencysearch/practicelevelsearch', new lang_string('practicelevelsearch', 'block_hpcl_competencysearch'),null, 0, 1));

    $settings->add(new admin_setting_configtext('block_hpcl_competencysearch/competencylist', new lang_string('competencylist', 'block_hpcl_competencysearch'),
        new lang_string('competencylisthint', 'block_hpcl_competencysearch'), null, PARAM_RAW));
    $settings->add(new admin_setting_configcheckbox('block_hpcl_competencysearch/competencysearch', new lang_string('competencysearch', 'block_hpcl_competencysearch'),null, 0, 1));
}


















