<?php

/**
/**
 * *************************************************************************
 * *                       HPCL - Competency Search                          **
 * *************************************************************************
 * @package     block                                                   **
 * @subpackage  HPCL competencysearch                                        **
 * @name        HPCL competency Search                                       **
 * @copyright   Dhruv Infoline Pvt Ltd                                  **
 * @author      Arjun Singh (arjunsingh@elearn10.com)                   **
 * @license     http://lmsofindia.com                                   **
 * *************************************************************************
 * ************************************************************************ */

defined('MOODLE_INTERNAL') || die();

function block_hpcl_competencysearch_course_tags($courseid,$q)
{
    global $CFG, $DB, $OUTPUT;
    $cat = '';
    $ret = '';
    $tagnameall = '';
    $taglinks = '<div class="tagname"><p><h6>'.get_string('search_keyword','block_hpcl_competencysearch').'</h6></p>';
    $count = 0;
    $tagresult = array();
    $filter_tags = explode(',', $q);
    $total = count($filter_tags);
    foreach ($filter_tags as $tagname) {
        $tagid = $DB->get_record('tag',array('name'=>$tagname),'id,name,tagcollid');
        if($tagid){
            $tagresult = $DB->get_record('tag_instance',array('itemid'=>$courseid,'itemtype' => 'course','tagid' => $tagid->id),'id,contextid');
            if($tagresult){
                $count = $count + 1;
            }
            $taglinks .= "<span><a target='_blank' class='label label-info standardtag' href='$CFG->wwwroot/tag/index.php?tc=$tagid->tagcollid&tag=$tagid->name&from=#core_course'>
                            &nbsp;$tagid->name
                            </a>
                        </span>";
        }
    }
    $taglinks .= "</div>";
    if($count >= $total){
        $coursename = $DB->get_record('course',array('id'=>$courseid));
        $ret .= '<div class="coursebox clearfix odd first"><div class="info">';
        $ret .= "<h4 class='coursename'><a href='$CFG->wwwroot/course/view.php?id=$courseid'>&nbsp;$coursename->fullname</a></h4>";
        $ret .=$taglinks;
        $ret .= '</div></div>';
    }
 
    return $ret;
}

function block_hpcl_competencysearch_searcharea($value)
{
    global $CFG, $DB, $OUTPUT;
    $formarea = '';
    require_once($CFG->dirroot.'/blocks/hpcl_competencysearch/search_form.php');
    $search_form = new block_hpcl_competencysearch_form();
    if($value != NULL ){
        $search_title = get_string('modify', 'block_hpcl_competencysearch');
    }else{
        $search_title = get_string('new_search', 'block_hpcl_competencysearch');
    }

    echo "<h4 class='search_title_new'>". $search_title."</h4>";
    if ($search_form->is_cancelled()) {
        redirect(new moodle_url('/blocks/hpcl_competencysearch/results.php'));
    } else if ($data = $search_form->get_data()) {
        
    }
    $formarea .= $search_form->display();
    $formarea .= "<hr/>";
    return $formarea;
}

function block_hpcl_competencysearch_othertags($q,$sbu,$role,$practicelevel,$competency)
{
    $othertags = array($sbu,$role,$practicelevel,$competency);
    $split_tag_names = array();
    if($q != '0'){
            $split_tag_names['q'] = $q; 
    }
    foreach ($othertags as $othertag) {
        if($othertag != '0'){
            if($othertag != NULL){
              $splitting = explode('_',$othertag);
            $split_tag_names[$splitting[0]] = $splitting[1];  
            }
        }
    }
    return implode(',',$split_tag_names);

}
