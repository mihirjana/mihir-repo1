<?php

/**
 * *************************************************************************
 * *                       HPCL - Competency Search                          **
 * *************************************************************************
 * @package     block                                                   **
 * @subpackage  HPCL competencysearch                                        **
 * @name        HPCL Competency Search                                       **
 * @copyright   Dhruv Infoline Pvt Ltd                                  **
 * @author      Arjun Singh (arjunsingh@elearn10.com)                   **
 * @license     http://lmsofindia.com                                   **
 * *************************************************************************
 * ************************************************************************ */

$string['hpcl_competencysearch:addinstance'] = 'Add a new hpcl competency search block';
$string['hpcl_competencysearch:myaddinstance'] = 'Add a new hpcl competency search block';
$string['new_search'] = 'New search';
$string['no_result'] = 'No result';
$string['pagetitle'] = 'HPCL Competency search results';
$string['pluginname'] = 'HPCL Competency search';
$string['course_results'] = 'Results for your search ';
$string['return_course'] = 'Return to course';
$string['searchfor'] = 'Enter keywords';
$string['settings'] = 'Settings';
$string['submit'] = 'Go';
$string['search'] = 'Search';
$string['modify'] = 'Course Search';
$string['cr'] = 'Courses';
$string['newsearch'] = 'New search';
$string['searchresult'] = 'Search results';
$string['competency_search'] = 'Competency Search';
$string['search_settings'] = 'Search Settings';
$string['sbulist'] = 'SBU List';
$string['rolelist'] = 'Role List';
$string['practicelevel'] = 'Practice level';
$string['practicelevel_help'] = 'Practice level filter. Adds a search filter with the search keyword, taken from the HPCL competency block settings.';
$string['competencylist'] = 'Competency list';
$string['sbulisthint'] = 'Enter the list of sbus to add in search filter';
$string['rolelisthint'] = 'Enter the list of roles to add in search filter';
$string['practicelevelhint'] = 'Enter the practice levels to add in search filter';
$string['competencylisthint'] = 'Enter the competency lists to add in search filter';
$string['q'] = 'Description';
$string['q_help'] = 'The search keyword description. This is the keywords to be entered as a search strings, which is searched based on the matching tagnames used in the different courses in site.';
$string['sbu'] = 'SBU';
$string['sbu_help'] = 'SBU filter. Adds a search filter with the search keyword, taken from the HPCL competency block settings.';
$string['role'] = 'Role';
$string['role_help'] = 'Roles filter. Adds a search filter with the search keyword, taken from the HPCL competency block settings.';
$string['competency'] = 'Competency';
$string['competency_help'] = 'Competency filter. Adds a search filter with the search keyword, taken from the HPCL competency block settings.';
$string['selectdefault'] = 'select an option';
$string['search_keyword'] = 'Search keyword(s):';

$string['competencysearch'] = 'Enable competency search';
$string['competencyhint'] = 'Check this to enable competency search';
$string['sbusearch'] = 'Enable sbu search';
$string['sbusearchhint'] = 'Check this to enable split search';
$string['rolesearch'] = 'Enable role search';
$string['rolesearchhint'] = 'Check this to enable split search';
$string['practicelevelsearch'] = 'Enable practice level search';
$string['practicelevelsearchhint'] = 'Check this to enable split search';