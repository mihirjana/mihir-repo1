<?php

/**
 * *************************************************************************
 * *                       HPCL - Competency Search Form                         **
 * *************************************************************************
 * @package     block                                                   **
 * @subpackage  cci competencysearch                                        **
 * @name        CCI Global Search                                       **
 * @copyright   Dhruv Infoline Pvt Ltd                                  **
 * @author      Arjun Singh (arjunsingh@elearn10.com)                   **
 * @license     http://lmsofindia.com                                   **
 * *************************************************************************
 * ************************************************************************ */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');
}
require_once($CFG->libdir.'/formslib.php');
class block_hpcl_competencysearch_form extends moodleform {

    function definition() {
        global $CFG,$DB;

        $mform =& $this->_form;
        $mform->addElement('text', 'q', get_string('searchfor','block_hpcl_competencysearch'),array() ,'size="20"');
        $mform->setType('q',PARAM_RAW);
        $mform->addHelpButton('q', 'q', 'block_hpcl_competencysearch');

        //$enablesearch = $DB->get_record('config_plugins',array('plugin'=>'block_hpcl_competencysearch','name'=>'competencysearch'),'value');
        
            $splitsearch = $DB->get_records('config_plugins',array('plugin'=>'block_hpcl_competencysearch'),'name,value');
            if($splitsearch){
            $selectdefault = get_string('selectdefault','block_hpcl_competencysearch');
            foreach ($splitsearch as $skey => $search) {
                if($search->name == 'sbulist'){
                    $sbulist[0] = $selectdefault;
                    $sbulists = explode(',',$search->value);
                    foreach ($sbulists as $key1 => $value1) {
                        $sbulist['sbu_'.$value1] = $value1;
                    }
                }else if($search->name == 'rolelist'){
                    $rolelist[0] = $selectdefault;
                    $rolelists = explode(',',$search->value);
                    foreach ($rolelists as $key2 => $value2) {
                        $rolelist['role_'.$value2] = $value2;
                    }
                }else if($search->name == 'practicelevel'){
                    $practicelevel[0] = $selectdefault;
                    $practicelevels = explode(',',$search->value);
                    foreach ($practicelevels as $key3 => $value3) {
                        $practicelevel['pl_'.$value3] = $value3;
                    }
                }else if($search->name == 'competencylist'){
                    $competencylist[0] = $selectdefault;
                    $competencylists = explode(',',$search->value);
                    foreach ($competencylists as $key4 => $value4) {
                        $competencylist['cmp_'.$value4] = $value4;
                    }
                }
                if($search->name == 'sbusearch' && $search->value == 1){
                    $mform->addElement('select', 'sbu', get_string('sbu', 'block_hpcl_competencysearch'), $sbulist);
                    $mform->setType('sbu',PARAM_RAW);
                    $mform->addHelpButton('sbu', 'sbu', 'block_hpcl_competencysearch');
                }
                if($search->name == 'rolesearch' && $search->value == 1){
                    $mform->addElement('select', 'role', get_string('role', 'block_hpcl_competencysearch'), $rolelist);
                    $mform->setType('role',PARAM_RAW);
                    $mform->addHelpButton('role', 'role', 'block_hpcl_competencysearch');
                }
                
                if($search->name == 'practicelevelsearch' && $search->value == 1){
                    $mform->addElement('select', 'practicelevel', get_string('practicelevel', 'block_hpcl_competencysearch'), $practicelevel);
                    $mform->setType('practicelevel',PARAM_RAW);
                    $mform->addHelpButton('practicelevel', 'practicelevel', 'block_hpcl_competencysearch');
                }
                if($search->name == 'competencysearch' && $search->value == 1){
                    $mform->addElement('select', 'competency', get_string('competency', 'block_hpcl_competencysearch'),$competencylist);
                    $mform->setType('competency',PARAM_RAW);
                    $mform->addHelpButton('competency', 'competency', 'block_hpcl_competencysearch');
                }    
            }
        }
        $this->add_action_buttons(false,get_string('search','block_hpcl_competencysearch'));
    }

    function validation($data, $files) {
        
    }
}
